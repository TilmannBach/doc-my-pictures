namespace Backend.Configurations
{
    public class DocMyPicsConfiguration
    {
        public const string DocMyPics = "DocMyPics";
        public string FontFile = "C:/Windows/Fonts/arialbd.ttf";
        public string Folder { get; set; }
        public string DbFile { get; set; }
        public int MinifiedImageWidth { get; set; }

        public string GetDbConnectionString => $"Filename={DbFile}";
    }
}