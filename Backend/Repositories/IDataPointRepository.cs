using System.Threading.Tasks;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Sieve.Models;

namespace Backend.Repositories
{
    public interface IDataPointRepository
    {
        Task<PagedResult<DataPoint>> GetDataPointsAsync(SieveModel sieveModel);
        Task<DataPoint> GetDataPointByIdAsync(int dataPointId);
        Task<DataPoint> CreateDataPointAsync(DataPoint newDataPoint);
        Task<DataPoint> UpdateDataPointAsync(DataPoint updateDataPoint);
        Task<DataPoint> SetDataPointVisibilityAsync(int idDataPoint, bool visibility);
    }
}