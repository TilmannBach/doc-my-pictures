using System.Linq;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Backend.Repositories
{
    class SeriesRepository : ISeriesRepository
    {
        private readonly DocMyPicturesContext _context;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly ILogger<SeriesRepository> _logger;

        public SeriesRepository(DocMyPicturesContext context, IOptions<SieveOptions> sieveOptions,
            ILogger<SeriesRepository> logger, ISieveProcessor sieveProcessor)
        {
            _context = context;
            _sieveOptions = sieveOptions.Value;
            _logger = logger;
            _sieveProcessor = sieveProcessor;
        }

        public async Task<PagedResult<Series>> GetSeriesAsync(SieveModel sieveModel)
        {
            var result = _context.Series.AsNoTracking()
                .Include(g => g.Grid);
            var pr = await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
            return pr;
        }

        public async Task<Series> GetSeriesByIdAsync(int seriesId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"Id=={seriesId.ToString()}",
                Sorts = "Id"
            };

            var result = await GetSeriesAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<Series> CreateSeriesAsync(Series newSeries)
        {
            var p = _context.Series.Add(newSeries);
            await _context.SaveChangesAsync();
            return p.Entity;
        }

        public async Task<Series> UpdateSeriesAsync(Series updateSeries)
        {
            _context.Series.Attach(updateSeries);
            _context.Entry(updateSeries).Property(p => p.Name).IsModified = true;
            _context.Entry(updateSeries).Property(p => p.Order).IsModified = true;

            await _context.SaveChangesAsync();

            return await GetSeriesByIdAsync(updateSeries.Id);
        }

        public async Task<Series> SetSeriesVisibilityMicroAsync(int idSeries, bool visibilityMicro)
        {
            var series = new Series
            {
                Id = idSeries,
                MicroIsVisible = visibilityMicro
            };

            _context.Series.Attach(series);
            _context.Entry(series).Property(p => p.MicroIsVisible).IsModified = true;

            await _context.SaveChangesAsync();

            return await GetSeriesByIdAsync(series.Id);
        }

        public async Task<Series> SetSeriesVisibilityMacroAsync(int idSeries, bool visibilityMacro)
        {
            var series = new Series
            {
                Id = idSeries,
                MacroIsVisible = visibilityMacro
            };

            _context.Series.Attach(series);
            _context.Entry(series).Property(p => p.MacroIsVisible).IsModified = true;

            await _context.SaveChangesAsync();

            return await GetSeriesByIdAsync(series.Id);
        }
    }
}