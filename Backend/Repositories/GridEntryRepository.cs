using System;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Backend.Repositories
{
    public class GridEntryRepository : IGridEntryRepository
    {
        private readonly DocMyPicturesContext _context;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;

        public GridEntryRepository(DocMyPicturesContext context, ISieveProcessor sieveProcessor,
            IOptions<SieveOptions> sieveOptions)
        {
            _context = context;
            _sieveProcessor = sieveProcessor;
            _sieveOptions = sieveOptions.Value;
        }

        public async Task<PagedResult<GridEntry>> GetGridEntriesAsync(SieveModel sieveModel)
        {
            var result = _context.GridEntries.AsNoTracking()
                .Include(ge => ge.Pictures)
                .Include(ge => ge.DataPoint)
                .Include(ge => ge.Series)
                .Include(ge => ge.Grid);
            return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
        }

        public Task<GridEntry> GetGridEntryByIdAsync(int gridEntryId)
        {
            throw new NotImplementedException();
        }

        public Task<GridEntry> UpdateGridEntryAsync(Grid updateGridEntry)
        {
            throw new NotImplementedException();
        }

        public async Task<int> CreateGridEntryAsync(GridEntry newGridEntry)
        {
            _context.GridEntries.Add(newGridEntry);
            await _context.SaveChangesAsync();
            return newGridEntry.Id;
        }
    }
}