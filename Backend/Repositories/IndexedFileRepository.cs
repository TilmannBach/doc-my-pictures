using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Backend.Messages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Backend.Repositories
{
    class IndexedFileRepository : IIndexedFileRepository
    {
        private readonly DocMyPicturesContext _context;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly ILogger<IndexedFileRepository> _logger;

        public IndexedFileRepository(DocMyPicturesContext context, IOptions<SieveOptions> sieveOptions,
            ILogger<IndexedFileRepository> logger, ISieveProcessor sieveProcessor)
        {
            _context = context;
            _sieveOptions = sieveOptions.Value;
            _logger = logger;
            _sieveProcessor = sieveProcessor;
        }

        public async Task<PagedResult<IndexedFile>> GetIndexedFilesAsync(SieveModel sieveModel)
        {
            var result = _context.IndexedFiles
                .AsNoTracking()
                .Include(f => f.Pictures)
                .ThenInclude<IndexedFile, Picture, GridEntry>(p => p.GridEntry);
            var pr = await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
            return pr;
        }

        public async Task<IndexedFile> GetIndexedFileByIdAsync(int indexedFileId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"id=={indexedFileId.ToString()}",
                Sorts = "Id"
            };

            var result = await GetIndexedFilesAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<IndexedFile> GetIndexedFileByPathAsync(string directory, string fileName)
        {
            var result = await _context.IndexedFiles
                .AsNoTracking()
                .Include(f => f.Pictures)
                .ThenInclude<IndexedFile, Picture, GridEntry>(p => p.GridEntry)
                .Where(f => f.Directory == directory && f.FileName == fileName)
                .ToListAsync();
            return result.SingleOrDefault();
        }

        public async Task<IndexedFile> CreateIndexedFileAsync(IndexedFile newIndexedFile)
        {
            var p = _context.IndexedFiles.Add(newIndexedFile);
            await _context.SaveChangesAsync();
            return p.Entity;
        }

        public async Task<IndexedFile> UpdateIndexedFileAttributesAsync(UpdateIndexedFile updateIndexedFile)
        {
            var file = new IndexedFile {Id = updateIndexedFile.Id};
            _context.IndexedFiles.Attach(file);

            if (updateIndexedFile.Hidden.HasValue)
            {
                file.Hidden = updateIndexedFile.Hidden.Value;
                _context.Entry(file).Property(f => f.Hidden).IsModified = true;
            }

            await _context.SaveChangesAsync();

            return await GetIndexedFileByIdAsync(file.Id);
        }

        public async Task<IList<FolderNameResponse>> GetIndexedFolderNamesAsync(SieveModel sieveModel)
        {
            var folders = _context.IndexedFiles
                .AsNoTracking()
                .GroupBy(indexedFile => indexedFile.Directory)
                .Select(g => new FolderNameResponse
                {
                    Directory = g.Key,
                    CountPictures = g.Count(v => true)
                });

            var filtered = _sieveProcessor.Apply(sieveModel, folders, applyPagination: false);
            return await filtered.ToListAsync();
        }
    }
}