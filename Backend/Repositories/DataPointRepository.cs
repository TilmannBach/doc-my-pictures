using System.Linq;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Backend.Repositories
{
    class DataPointRepository : IDataPointRepository
    {
        private readonly DocMyPicturesContext _context;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly ILogger<DataPointRepository> _logger;

        public DataPointRepository(DocMyPicturesContext context, IOptions<SieveOptions> sieveOptions,
            ILogger<DataPointRepository> logger, ISieveProcessor sieveProcessor)
        {
            _context = context;
            _sieveOptions = sieveOptions.Value;
            _logger = logger;
            _sieveProcessor = sieveProcessor;
        }

        public async Task<PagedResult<DataPoint>> GetDataPointsAsync(SieveModel sieveModel)
        {
            var result = _context.DataPoints.AsNoTracking()
                .Include(g => g.Grid);
            var pr = await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
            return pr;
        }

        public async Task<DataPoint> GetDataPointByIdAsync(int dataPointId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"Id=={dataPointId.ToString()}",
                Sorts = "Id"
            };

            var result = await GetDataPointsAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<DataPoint> CreateDataPointAsync(DataPoint newDataPoint)
        {
            var p = _context.DataPoints.Add(newDataPoint);
            await _context.SaveChangesAsync();
            return p.Entity;
        }

        public async Task<DataPoint> UpdateDataPointAsync(DataPoint updateDataPoint)
        {
            _context.DataPoints.Attach(updateDataPoint);
            _context.Entry(updateDataPoint).Property(p => p.Name).IsModified = true;
            _context.Entry(updateDataPoint).Property(p => p.Order).IsModified = true;

            await _context.SaveChangesAsync();

            return await GetDataPointByIdAsync(updateDataPoint.Id);
        }

        public async Task<DataPoint> SetDataPointVisibilityAsync(int idDataPoint, bool visibility)
        {
            var dp = new DataPoint
            {
                Id = idDataPoint,
                IsVisible = visibility
            };

            _context.DataPoints.Attach(dp);
            _context.Entry(dp).Property(p => p.IsVisible).IsModified = true;

            await _context.SaveChangesAsync();

            return await GetDataPointByIdAsync(dp.Id);
        }
    }
}