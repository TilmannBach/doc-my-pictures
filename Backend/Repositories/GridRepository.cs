using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Backend.Repositories
{
    public class GridRepository : IGridRepository
    {
        private readonly DocMyPicturesContext _context;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly ILogger<GridRepository> _logger;

        public GridRepository(DocMyPicturesContext context, IOptions<SieveOptions> sieveOptions,
            ILogger<GridRepository> logger, ISieveProcessor sieveProcessor)
        {
            _context = context;
            _sieveOptions = sieveOptions.Value;
            _logger = logger;
            _sieveProcessor = sieveProcessor;
        }

        public async Task<PagedResult<Grid>> GetGridsAsync(SieveModel sieveModel)
        {
            var result = _context.Grids.AsNoTracking()
                .Include(g => g.Series.OrderBy(s => s.Order))
                .Include(g => g.DataPoints.OrderBy(dp => dp.Order))
                .Include(g => g.GridEntries)
                .ThenInclude(ge => ge.Pictures.OrderByDescending(p=> p.PictureType).ThenBy(p=>p.Order))
                .ThenInclude(p => p.IndexedFile)
                .Include(g => g.GridEntries)
                .ThenInclude(ge => ge.Series)
                .Include(g => g.GridEntries)
                .ThenInclude(ge => ge.DataPoint);
            var pr = await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
            return pr;
        }

        public async Task<Grid> GetGridByIdAsync(int gridId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"Id=={gridId.ToString()}",
                Sorts = "Id"
            };

            var result = await GetGridsAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<Grid> CreateGridAsync(Grid newGrid)
        {
            var p = _context.Grids.Add(newGrid);
            await _context.SaveChangesAsync();
            return p.Entity;
        }

        public async Task<Grid> UpdateGridAsync(Grid updateGrid)
        {
            _context.Grids.Attach(updateGrid);
            _context.Entry(updateGrid).Property(p => p.Name).IsModified = true;

            await _context.SaveChangesAsync();

            return await GetGridByIdAsync(updateGrid.Id);
        }
    }
}