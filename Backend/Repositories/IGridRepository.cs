using System.Threading.Tasks;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Sieve.Models;

namespace Backend.Repositories
{
    public interface IGridRepository
    {
        Task<PagedResult<Grid>> GetGridsAsync(SieveModel sieveModel);
        Task<Grid> GetGridByIdAsync(int gridId);
        Task<Grid> CreateGridAsync(Grid newGrid);
        Task<Grid> UpdateGridAsync(Grid updateGrid);
    }
}