using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Backend.Messages;
using Sieve.Models;

namespace Backend.Repositories
{
    public interface IIndexedFileRepository
    {
        Task<PagedResult<IndexedFile>> GetIndexedFilesAsync(SieveModel sieveModel);
        Task<IndexedFile> GetIndexedFileByIdAsync(int indexedFileId);
        Task<IndexedFile> GetIndexedFileByPathAsync(string directory, string fileName);
        Task<IndexedFile> CreateIndexedFileAsync(IndexedFile newIndexedFile);
        Task<IndexedFile> UpdateIndexedFileAttributesAsync(UpdateIndexedFile updateIndexedFile);
        Task<IList<FolderNameResponse>> GetIndexedFolderNamesAsync(SieveModel sieveModel);
    }
}