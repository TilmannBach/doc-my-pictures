using System.Threading.Tasks;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Sieve.Models;

namespace Backend.Repositories
{
    public interface ISeriesRepository
    {
        Task<PagedResult<Series>> GetSeriesAsync(SieveModel sieveModel);
        Task<Series> GetSeriesByIdAsync(int seriesId);
        Task<Series> CreateSeriesAsync(Series newSeries);
        Task<Series> UpdateSeriesAsync(Series updateSeries);
        Task<Series> SetSeriesVisibilityMicroAsync(int idSeries, bool visibilityMicro);
        Task<Series> SetSeriesVisibilityMacroAsync(int idSeries, bool visibilityMacro);
    }
}