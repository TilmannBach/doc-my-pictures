using System.Threading.Tasks;
using Backend.Entities;

namespace Backend.Repositories
{
    public interface IPictureRepository
    {
        Task CreatePictureAsync(Picture newPicture);
        Task<Picture> GetByIdAsync(int pictureId);

        Task DeleteAsync(Picture picture);

        Task<Picture> UpdateAsync(Picture updatedPicture);
        Task<Picture> UpdateFavoriteAsync(Picture updatedPicture);
        Task<Picture> UpdateOrderAsync(Picture updatedPicture);
        Task<Picture> UpdatePictureTypeAsync(Picture updatedPicture);
    }
}