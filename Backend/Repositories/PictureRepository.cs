using System.Linq;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Backend.Repositories
{
    public class PictureRepository : IPictureRepository
    {
        private readonly DocMyPicturesContext _context;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;

        public PictureRepository(DocMyPicturesContext context, IOptions<SieveOptions> sieveOptions,
            ISieveProcessor sieveProcessor)
        {
            _context = context;
            _sieveOptions = sieveOptions.Value;
            _sieveProcessor = sieveProcessor;
        }

        public async Task CreatePictureAsync(Picture newPicture)
        {
            _context.Pictures.Add(newPicture);
            await _context.SaveChangesAsync();
        }

        public async Task<PagedResult<Picture>> GetPicturesAsync(SieveModel sieveModel)
        {
            sieveModel.Sorts ??= "Order";
            var result = _context.Pictures.AsNoTracking()
                .Include(p => p.GridEntry)
                .Include(p => p.IndexedFile);
            var pr = await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
            return pr;
        }

        public async Task<Picture> GetByIdAsync(int pictureId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"Id=={pictureId}",
                Sorts = "Id"
            };
            var result = await GetPicturesAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task DeleteAsync(Picture picture)
        {
            _context.Pictures.Remove(picture);
            await _context.SaveChangesAsync();
        }

        public async Task<Picture> UpdateAsync(Picture updatedPicture)
        {
            _context.Pictures.Attach(updatedPicture);
            _context.Entry(updatedPicture).Property(p => p.IsFavorite).IsModified = true;
            _context.Entry(updatedPicture).Property(p => p.Order).IsModified = true;
            _context.Entry(updatedPicture).Property(p => p.PictureType).IsModified = true;
            await _context.SaveChangesAsync();

            return await GetByIdAsync(updatedPicture.Id);
        }

        public async Task<Picture> UpdateFavoriteAsync(Picture updatedPicture)
        {
            _context.Pictures.Attach(updatedPicture);
            _context.Entry(updatedPicture).Property(p => p.IsFavorite).IsModified = true;
            await _context.SaveChangesAsync();

            return await GetByIdAsync(updatedPicture.Id);
        }

        public async Task<Picture> UpdateOrderAsync(Picture updatedPicture)
        {
            _context.Pictures.Attach(updatedPicture);
            _context.Entry(updatedPicture).Property(p => p.Order).IsModified = true;
            await _context.SaveChangesAsync();

            return await GetByIdAsync(updatedPicture.Id);
        }

        public async Task<Picture> UpdatePictureTypeAsync(Picture updatedPicture)
        {
            _context.Pictures.Attach(updatedPicture);
            _context.Entry(updatedPicture).Property(p => p.PictureType).IsModified = true;
            await _context.SaveChangesAsync();

            return await GetByIdAsync(updatedPicture.Id);
        }
    }
}