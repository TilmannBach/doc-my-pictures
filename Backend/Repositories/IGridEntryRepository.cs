using System.Threading.Tasks;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Sieve.Models;

namespace Backend.Repositories
{
    public interface IGridEntryRepository
    {
        Task<PagedResult<GridEntry>> GetGridEntriesAsync(SieveModel sieveModel);
        Task<GridEntry> GetGridEntryByIdAsync(int gridEntryId);
        /// <summary>
        /// Update macht create und update gleichzeitig.
        /// </summary>
        /// <param name="updateGridEntry"></param>
        /// <returns></returns>
        Task<GridEntry> UpdateGridEntryAsync(Grid updateGridEntry);

        Task<int> CreateGridEntryAsync(GridEntry newGridEntry);
    }
}