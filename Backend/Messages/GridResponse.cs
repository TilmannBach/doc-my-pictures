using System.Collections.Generic;

namespace Backend.Messages
{
    public class GridResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IList<SeriesResponse> Series { get; set; }
        public IList<DataPointResponse> DataPoints { get; set; }
        public IList<GridEntryResponse> GridEntries { get; set; }
    }
}