namespace Backend.Messages
{
    public class UpdateGrid : BaseUpdateMessage
    {
        public string Name { get; set; }
    }
}