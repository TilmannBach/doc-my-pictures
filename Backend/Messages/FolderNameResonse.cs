using Sieve.Attributes;

namespace Backend.Messages
{
    public class FolderNameResponse
    {
        [Sieve(CanFilter = true, CanSort = true)]
        public string Directory { get; set; }

        public int CountPictures { get; set; }
    }
}