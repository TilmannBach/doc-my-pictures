using System.Linq;
using Backend.Entities;
using Mapster;

namespace Backend.Messages.Mappings
{
    public class MessagesMapsterRegister : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Grid, GridResponse>()
                .MaxDepth(5)
                .Map( // todo check if this mapping is still needed!
                    g => g.Series,
                    g => g.Series.OrderBy(s => s.Order)
                )
                .Map( // todo check if this mapping is still needed!
                    g => g.DataPoints,
                    g => g.DataPoints.OrderBy(dp => dp.Order)
                )
                ;

            config.NewConfig<DataPoint, DataPointResponse>()
                .MaxDepth(5);

            config.NewConfig<Series, SeriesResponse>()
                .MaxDepth(5);

            config.NewConfig<IndexedFile, IndexedFileResponse>()
                .MaxDepth(5);

            config.NewConfig<Picture, PictureResponse>()
                .MaxDepth(5);
        }
    }
}