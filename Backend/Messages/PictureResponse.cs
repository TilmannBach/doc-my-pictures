using Backend.Entities;

namespace Backend.Messages
{
    public class PictureResponse
    {
        public int Id { get; set; }

        public bool IsFavorite { get; set; } = false;

        public int IndexedFileId { get; set; }

        public int Order { get; set; }

        public IndexedFileResponse IndexedFile { get; set; }

        public GridEntryResponse GridEntry { get; set; }

        public PictureType PictureType { get; set; }
    }
}