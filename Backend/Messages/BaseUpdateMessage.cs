namespace Backend.Messages
{
    public abstract class BaseUpdateMessage
    {
        internal int Id { get; set; }
    }
}