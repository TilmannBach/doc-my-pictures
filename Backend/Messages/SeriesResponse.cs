namespace Backend.Messages
{
    public class SeriesResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public GridResponse Grid { get; set; }

        public bool MicroIsVisible { get; set; }
        public bool MacroIsVisible { get; set; }
    }
}