namespace Backend.Messages
{
    public class CreateGridEntry
    {
        public int SeriesId { get; set; }

        public int DataPointId { get; set; }

        public int PictureId { get; set; }
    }
}