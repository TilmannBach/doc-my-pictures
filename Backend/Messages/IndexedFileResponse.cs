using System;
using System.Collections.Generic;

namespace Backend.Messages
{
    public class IndexedFileResponse
    {
        public int Id { get; set; }

        public string Directory { get; set; }

        public string FileName { get; set; }

        public bool Hidden { get; set; } = false;

        public IList<PictureResponse> Pictures { get; set; }

        public DateTime DateAdded { get; set; }
        
        public DateTime DateCreated { get; set; }
    }
}