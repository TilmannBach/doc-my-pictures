using System.Collections.Generic;

namespace Backend.Messages
{
    public class GridEntryResponse
    {
        public int Id { get; set; }

        public int GridId { get; set; }

        public SeriesResponse Series { get; set; }

        public DataPointResponse DataPoint { get; set; }

        public IList<PictureResponse> Pictures { get; set; }
    }
}