using Backend.Entities;

namespace Backend.Messages
{
    public record AssignIndexedPicture (
        int DataPointId,
        int SeriesId,
        int IndexedFileId,
        PictureType PictureType,
        int Order
    );

}