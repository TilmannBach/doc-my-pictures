namespace Backend.Messages
{
    public class UpdateGridEntry : BaseUpdateMessage
    {
        public int SeriesId { get; set; }

        public int DataPointId { get; set; }

        public int PictureId { get; set; }
    }
}