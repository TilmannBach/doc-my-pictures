using System.ComponentModel.DataAnnotations;
using Backend.Entities;

namespace Backend.Messages
{
    public class DataPointResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public GridResponse Grid { get; set; }

        public bool IsVisible { get; set; }
    }
}