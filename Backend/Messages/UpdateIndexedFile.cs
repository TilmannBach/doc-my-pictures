namespace Backend.Messages
{
    public class UpdateIndexedFile : BaseUpdateMessage
    {
        public bool? Hidden { get; set; }
    }
}