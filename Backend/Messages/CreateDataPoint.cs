namespace Backend.Messages
{
    public class CreateDataPoint
    {
        public string Name { get; set; }

        public int Order { get; set; }

        public int GridId { get; set; }
    }
}