using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sieve.Models;
using Sieve.Services;

namespace Backend.Helpers.Sieve
{
    public static class SieveProcessorExtensions
    {
        public static async Task<PagedResult<T>> GetPagedAsync<T>(this ISieveProcessor sieveProcessor,
            IQueryable<T> query, SieveOptions sieveOptions, SieveModel sieveModel) where T : class
        {
            var result = new PagedResult<T>();

            var (pagedQuery, page, pageSize, rowCount, pageCount) =
                await GetPagedResultAsync(sieveProcessor, query, sieveOptions, sieveModel);

            result.CurrentPage = page;
            result.PageSize = pageSize;
            result.RowCount = rowCount;
            result.PageCount = pageCount;

            result.Results = await pagedQuery.ToListAsync();

            return result;
        }

        private static async Task<(IQueryable<T> pagedQuery, int page, int pageSize, int rowCount, int pageCount)>
            GetPagedResultAsync<T>(ISieveProcessor sieveProcessor, IQueryable<T> query, SieveOptions sieveOptions,
                SieveModel sieveModel)
            where T : class
        {
            var page = sieveModel.Page ?? 1;
            var pageSize = sieveModel.PageSize ?? sieveOptions.DefaultPageSize;
            pageSize = pageSize <= sieveOptions.MaxPageSize ? pageSize : sieveOptions.MaxPageSize;

            // apply pagination in a later step
            query = sieveProcessor.Apply(sieveModel, query, applyPagination: false);

            var rowCount = await query.CountAsync();

            var pageCount = (int) Math.Ceiling((double) rowCount / pageSize);

            var skip = (page - 1) * pageSize;
            var pagedQuery = query.Skip(skip).Take(pageSize);

            return (pagedQuery, page, pageSize, rowCount, pageCount);
        }
    }
}