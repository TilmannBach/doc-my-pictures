using System.Collections.Generic;

namespace Backend.Helpers.Sieve
{
    public interface IPagedResult<T>
    {
        int CurrentPage { get;  }
        int PageCount { get; }
        int PageSize { get;  }
        long RowCount { get;  }

        IList<T> Results { get; }
    }
}