using System.Linq;
using Backend.Entities;
using Sieve.Services;

namespace Backend.Helpers.Sieve
{
    public class SieveCustomFilterMethods : ISieveCustomFilterMethods
    {
        public IQueryable<IndexedFile> Usages(IQueryable<IndexedFile> source, string op, string[] values)
        {
            if (!int.TryParse(values[0], out var value))
            {
                return source;
            }

            return op switch
            {
                "==" => source.Where(f => f.Pictures.Count == value),
                "!=" => source.Where(f => f.Pictures.Count != value),
                ">" => source.Where(f => f.Pictures.Count > value),
                "<" => source.Where(f => f.Pictures.Count < value),
                ">=" => source.Where(f => f.Pictures.Count >= value),
                "<=" => source.Where(f => f.Pictures.Count <= value),
                _ => source
            };
        }
    }
}