using Backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace Backend.Contexts
{
    public class DocMyPicturesContext : DbContext
    {
        public DbSet<DataPoint> DataPoints { get; set; }
        public DbSet<Grid> Grids { get; set; }
        public DbSet<GridEntry> GridEntries { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Series> Series { get; set; }
        public DbSet<IndexedFile> IndexedFiles { get; set; }

        public DocMyPicturesContext(DbContextOptions<DocMyPicturesContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlite(o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Grid>()
                .HasIndex(g => g.Name).IsUnique();

            modelBuilder.Entity<GridEntry>()
                .HasIndex(p => new {p.SeriesId, p.DataPointId}).IsUnique();

            modelBuilder.Entity<IndexedFile>()
                .HasIndex(f => new {f.Directory, f.FileName}).IsUnique();
        }
    }
}