using System.IO;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using Backend.Configurations;
using Backend.Contexts;
using Backend.Filters;
using Backend.Helpers;
using Backend.Helpers.Sieve;
using Backend.Repositories;
using Backend.Services;
using Mapster;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Sieve.Models;
using Sieve.Services;

namespace Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // TypeAdapterConfig.GlobalSettings.RequireExplicitMapping = true;
            // TypeAdapterConfig.GlobalSettings.RequireDestinationMemberSource = true;
            TypeAdapterConfig.GlobalSettings.Default.Settings.EnableNonPublicMembers = true;
            TypeAdapterConfig.GlobalSettings.Scan(Assembly.GetExecutingAssembly());
            TypeAdapterConfig.GlobalSettings.Compile();

            var configSection = Configuration.GetSection(DocMyPicsConfiguration.DocMyPics);
            services.Configure<DocMyPicsConfiguration>(configSection);

            services.AddScoped<ISieveProcessor, ApplicationSieveProcessor>();
            services.Configure<SieveOptions>(Configuration.GetSection("Sieve"));
            services.AddScoped<ISieveCustomSortMethods, SieveCustomSortMethods>();
            services.AddScoped<ISieveCustomFilterMethods, SieveCustomFilterMethods>();
            services.AddDbContext<DocMyPicturesContext>(options => { options.UseSqlite(configSection.Get<DocMyPicsConfiguration>().GetDbConnectionString); });

            services.AddControllers(options =>
                {
                    options.Filters.Add(typeof(MapIdToUpdateMessageFilter), MapIdToUpdateMessageFilter.FilterOrder);
                    options.Filters.Add(typeof(PagedResultResponseFilter));
                })
                .AddJsonOptions(
                    options =>
                    {
                        options.JsonSerializerOptions.NumberHandling =
                            JsonNumberHandling.Strict; // TODO or go with the default behavior?
                        options.JsonSerializerOptions.Converters.Add(new DateTimeToAtomStringConverter());
                        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                        // options.JsonSerializerOptions.IgnoreNullValues = true;
                        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressInferBindingSourcesForParameters = false; // not disable binding source inference
                    options.SuppressMapClientErrors = false; // automatically create ProblemDetails
                });

            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "Backend", Version = "v1"}); });

            services.AddScoped<IIndexedFileRepository, IndexedFileRepository>();
            services.AddScoped<IGridRepository, GridRepository>();
            services.AddScoped<IDataPointRepository, DataPointRepository>();
            services.AddScoped<ISeriesRepository, SeriesRepository>();
            services.AddScoped<IGridEntryRepository, GridEntryRepository>();
            services.AddScoped<IPictureRepository, PictureRepository>();

            services.AddHostedService<FileIndexer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DocMyPicturesContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Backend v1"));
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            // serve static files from wwwroot
            app.UseDefaultFiles();
            var fileExtensionContentTypeProvider = new FileExtensionContentTypeProvider
            {
                Mappings = {[".webmanifest"] = "application/manifest+json"}
            };
            app.UseStaticFiles(new StaticFileOptions
            {
                ContentTypeProvider = fileExtensionContentTypeProvider
            });

            if (!env.IsDevelopment())
            {
                app.MapWhen(context => !context.Request.Path.StartsWithSegments(PathString.FromUriComponent("/api")), spaApp =>
                {
                    spaApp.UseSpa(spa =>
                    {
                        spa.Options.SourcePath = Path.Combine("wwwroot");
                        spa.Options.DefaultPageStaticFileOptions = new StaticFileOptions
                        {
                            FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),
                            ContentTypeProvider = fileExtensionContentTypeProvider
                        };
                    });
                });
            }
        }
    }
}