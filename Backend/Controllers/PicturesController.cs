using System.Linq;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Messages;
using Backend.Repositories;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PicturesController : ControllerBase
    {
        private readonly IGridEntryRepository _gridEntryRepo;
        private readonly IPictureRepository _pictureRepo;
        private readonly ISeriesRepository _seriesRepository;


        public PicturesController(IGridEntryRepository gridEntryRepo, IPictureRepository pictureRepo,
            ISeriesRepository seriesRepository)
        {
            _gridEntryRepo = gridEntryRepo;
            _pictureRepo = pictureRepo;
            _seriesRepository = seriesRepository;
        }

        [HttpPost("assign")]
        public async Task<ActionResult<PictureResponse>> AssignIndexedPicture([FromBody] AssignIndexedPicture body)
        {
            var geSieveModel = new SieveModel
            {
                Filters = $"SeriesId=={body.SeriesId},DataPointId=={body.DataPointId}" // maybe add the GridId?
            };
            var gePagedResult = await _gridEntryRepo.GetGridEntriesAsync(geSieveModel);
            GridEntry gridEntry;
            if (gePagedResult.RowCount == 0)
            {
                var series = await _seriesRepository.GetSeriesByIdAsync(body.SeriesId);
                gridEntry = new GridEntry
                {
                    SeriesId = body.SeriesId,
                    DataPointId = body.DataPointId,
                    GridId = series.GridId,
                };
                await _gridEntryRepo.CreateGridEntryAsync(gridEntry);
            }
            else
            {
                gridEntry = gePagedResult.Results.First();
            }

            var newPicture = new Picture
            {
                GridEntryId = gridEntry.Id,
                Order = body.Order,
                IndexedFileId = body.IndexedFileId,
                PictureType = body.PictureType,
            };
            await _pictureRepo.CreatePictureAsync(newPicture);
            var picture = await _pictureRepo.GetByIdAsync(newPicture.Id);
            return Ok(picture.Adapt<PictureResponse>());
        }

        [HttpDelete("{pictureId:int}")]
        public async Task<ActionResult> DeletePicture([FromRoute] int pictureId)
        {
            var picture = await _pictureRepo.GetByIdAsync(pictureId);
            if (picture is null)
            {
                return NotFound();
            }

            await _pictureRepo.DeleteAsync(picture);
            return NoContent();
        }

        [HttpPut("{pictureId:int}/favorite")]
        public async Task<ActionResult> MarkAsFav([FromRoute] int pictureId)
        {
            var picture = await _pictureRepo.GetByIdAsync(pictureId);
            if (picture is null)
            {
                return NotFound();
            }

            picture.IsFavorite = true;
            await _pictureRepo.UpdateFavoriteAsync(picture);
            return NoContent();
        }

        [HttpDelete("{pictureId:int}/favorite")]
        public async Task<ActionResult> UnmarkAsFav([FromRoute] int pictureId)
        {
            var picture = await _pictureRepo.GetByIdAsync(pictureId);
            if (picture is null)
            {
                return NotFound();
            }

            picture.IsFavorite = false;
            await _pictureRepo.UpdateFavoriteAsync(picture);
            return NoContent();
        }

        [HttpPut("{pictureId:int}/order/{orderValue:int}")]
        public async Task<ActionResult> OrderPicture([FromRoute] int pictureId, [FromRoute] int orderValue)
        {
            var picture = await _pictureRepo.GetByIdAsync(pictureId);
            if (picture is null)
            {
                return NotFound();
            }

            picture.Order = orderValue;
            await _pictureRepo.UpdateOrderAsync(picture);
            return NoContent();
        }

        [HttpPut("{pictureId:int}/type/{type}")]
        public async Task<ActionResult> ChangePictureType([FromRoute] int pictureId, [FromRoute] PictureType type)
        {
            var picture = await _pictureRepo.GetByIdAsync(pictureId);
            if (picture is null)
            {
                return NotFound();
            }

            picture.PictureType = type;
            await _pictureRepo.UpdatePictureTypeAsync(picture);
            return NoContent();
        }
    }
}