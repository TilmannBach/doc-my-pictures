using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Backend.Configurations;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Backend.Messages;
using Backend.Repositories;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sieve.Models;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GridsController : ControllerBase
    {
        private readonly IGridRepository _gridRepository;
        private readonly ILogger<GridsController> _logger;
        private readonly DocMyPicsConfiguration _config;

        private const int YLabelPx = 200;
        private const int PicsXWidth = 768;
        private const int PicsYHeight = 576;

        public GridsController(
            ILogger<GridsController> logger,
            IGridRepository gridRepository,
            IOptions<DocMyPicsConfiguration> options
        )
        {
            _logger = logger;
            _gridRepository = gridRepository;
            _config = options.Value;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<GridResponse>>> GetGrids([FromQuery] SieveModel sieveModel)
        {
            sieveModel.Sorts ??= "Name";

            var result = await _gridRepository.GetGridsAsync(sieveModel);

            return Ok(result.Adapt<PagedResult<GridResponse>>());
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GridResponse>> CreateGrid([FromBody] CreateGrid createGridEntry)
        {
            var newGrid = createGridEntry.Adapt<Grid>();
            var grid = await _gridRepository.CreateGridAsync(newGrid);

            return CreatedAtAction(nameof(GetGrids), new {filters = $"id=={grid.Id}"}, grid.Adapt<GridResponse>());
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GridResponse>> UpdateGrid([FromBody] UpdateGrid updateGrid, [FromRoute] int id)
        {
            var grid = updateGrid.Adapt<Grid>();
            await _gridRepository.UpdateGridAsync(grid);

            return Ok(grid.Adapt<GridResponse>());
        }

        [HttpPost("{id:int}/create-transformation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GridResponse>> CreateTransformedGrid([FromRoute] int id)
        {
            var oldGrid = await _gridRepository.GetGridByIdAsync(id);

            var newGrid = new Grid
            {
                Name = $"{oldGrid.Name} (transformed)"
            };
            
            // series
            var newSeriesList = new Dictionary<int, Series>();
            for (var index = 0; index < oldGrid.Series.Count; index++)
            {
                var oldGridSeries = oldGrid.Series[index];
                var newDp = new DataPoint
                {
                    Name = oldGridSeries.Name,
                    Order = oldGridSeries.Order,
                    IsVisible = oldGridSeries.MacroIsVisible,
                };
                newGrid.DataPoints.Add(newDp);

                // datapoints
                for (var i = 0; i < oldGrid.DataPoints.Count; i++)
                {
                    var oldGridDataPoint = oldGrid.DataPoints[i];
                    if (index == 0)
                    {
                        var newSeries = new Series
                        {
                            Name = oldGridDataPoint.Name,
                            Order = oldGridDataPoint.Order,
                            MacroIsVisible = oldGridDataPoint.IsVisible,
                            MicroIsVisible = oldGridDataPoint.IsVisible,
                        };
                        newGrid.Series.Add(newSeries);
                        newSeriesList[i] = newSeries;
                    }

                    // gridEntries
                    var ges = oldGrid.GridEntries.Where(ge => ge.SeriesId == oldGridSeries.Id && ge.DataPointId == oldGridDataPoint.Id).ToList();
                    if (!ges.Any()) continue;

                    var oldGe = ges.First();
                    var newGe = new GridEntry
                    {
                        Series = newSeriesList[i],
                        DataPoint = newDp,
                        Pictures = oldGe.Pictures.Select(oldPic =>
                        {
                            var newPic = new Picture
                            {
                                Order = oldPic.Order,
                                IsFavorite = oldPic.IsFavorite,
                                PictureType = oldPic.PictureType,
                                IndexedFileId = oldPic.IndexedFileId,
                            };
                            return newPic;
                        }).ToList()
                    };
                    newGrid.GridEntries.Add(newGe);
                }
            }

            var savedGrid = await _gridRepository.CreateGridAsync(newGrid);
            return Ok(savedGrid.Adapt<GridResponse>());
        }

        [HttpGet("{id:int}/final-picture")]
        public async Task<ActionResult> FinalPicture([FromRoute] int id, [FromQuery] int fontSize = 72)
        {
            var grid = await _gridRepository.GetGridByIdAsync(id);

            var datapointCountVisible = grid.DataPoints.Count(dp => dp.IsVisible);
            var seriesMicroMacroVisible = grid.Series.Sum(s => CountTrue(s.MacroIsVisible, s.MicroIsVisible));

            var longestDpLabel = grid.DataPoints.Max(dp => dp.Name.Length)!;

            var xLabelPx = longestDpLabel * 35 + 70;


            var width = PicsXWidth * seriesMicroMacroVisible + xLabelPx;
            var height = PicsYHeight * datapointCountVisible + YLabelPx;

            Image output = new Image<Rgba32>(width, height, Rgba32.ParseHex("ffffff"));

            var cumulatedX = xLabelPx;
            var cumulatedY = YLabelPx;

            var collection = new FontCollection();
            var family = collection.Install(_config.FontFile);
            var font = family.CreateFont(fontSize, FontStyle.Regular);

            foreach (var gridDataPoint in grid.DataPoints.Where(dp => dp.IsVisible))
            {
                var textSizeDataPoint = TextMeasurer.Measure(gridDataPoint.Name, new RendererOptions(font));
                output.Mutate(ipc =>
                    ipc.DrawText(gridDataPoint.Name, font, Color.Black, new PointF(5, (int) (cumulatedY + PicsYHeight / 2 - textSizeDataPoint.Height / 2))));

                foreach (var gridSeries in grid.Series.Where(s => s.MacroIsVisible || s.MicroIsVisible))
                {
                    var countMarcoMicro = CountTrue(gridSeries.MacroIsVisible, gridSeries.MicroIsVisible);

                    var textSizeSeries = TextMeasurer.Measure(gridSeries.Name, new RendererOptions(font));
                    output.Mutate(ipc => ipc.DrawText(
                            gridSeries.Name,
                            font,
                            Color.Black,
                            new PointF(
                                CalcSeriesLabelXPoint(cumulatedX, countMarcoMicro, textSizeSeries),
                                CalcSeriesLabelY(YLabelPx, textSizeSeries)
                            )
                        )
                    );

                    var gridEntry = grid.GridEntries.FirstOrDefault(ge => ge.SeriesId == gridSeries.Id && ge.DataPointId == gridDataPoint.Id);

                    if (gridEntry == null)
                    {
                        /*
                        | Ma | Mi |
                        | Ma |
                        | Mi |
                        */
                        cumulatedX += countMarcoMicro * PicsXWidth;
                        continue;
                    }

                    if (gridSeries.MacroIsVisible)
                    {
                        // MACRO
                        var pictureMacro = gridEntry.Pictures.FirstOrDefault(p => p.IsFavorite && p.PictureType == PictureType.Macro);
                        if (pictureMacro == null)
                        {
                            cumulatedX += PicsXWidth;
                        }
                        else
                        {
                            var pathMacro = Path.Combine(_config.Folder, pictureMacro.IndexedFile.Directory, pictureMacro.IndexedFile.FileName);
                            var imageMacro = await Image.LoadAsync(pathMacro);
                            imageMacro.Mutate(o => { o.Resize(0, PicsYHeight); });
                            var xMacro = cumulatedX;
                            var yMacro = cumulatedY;
                            output.Mutate(o => { o.DrawImage(imageMacro, new Point(xMacro, yMacro), 1); });
                            cumulatedX += PicsXWidth;
                        }
                    }

                    if (!gridSeries.MicroIsVisible) continue;

                    // MICRO
                    var pictureMicro = gridEntry.Pictures.FirstOrDefault(p => p.IsFavorite && p.PictureType == PictureType.Micro);

                    if (pictureMicro == null)
                    {
                        cumulatedX += PicsXWidth;
                        continue;
                    }


                    var pathMicro = Path.Combine(_config.Folder, pictureMicro.IndexedFile.Directory, pictureMicro.IndexedFile.FileName);
                    var image = await Image.LoadAsync(pathMicro);
                    image.Mutate(o => { o.Resize(0, PicsYHeight); });

                    var x = cumulatedX;
                    var y = cumulatedY;
                    output.Mutate(o => { o.DrawImage(image, new Point(x, y), 1); });
                    cumulatedX += PicsXWidth;
                }

                cumulatedX = xLabelPx;
                cumulatedY += PicsYHeight;
            }

            var fileFormat = PngFormat.Instance;
            Stream outputStream = new MemoryStream();

            await output.SaveAsync(outputStream, fileFormat);
            outputStream.Seek(0, SeekOrigin.Begin);

            return File(outputStream, fileFormat.DefaultMimeType);
        }

        private static int CalcSeriesLabelY(int yLabelPx, FontRectangle textSizeSeries)
        {
            var y = (int) ((int) (yLabelPx / 2) - textSizeSeries.Height / 2);
            return y;
        }

        private int CalcSeriesLabelXPoint(int cumulatedX, int countMacroMicro, FontRectangle textSizeSeries)
        {
            var seriesPicsWidth = PicsXWidth * countMacroMicro;

            var labelPos = (int)((textSizeSeries.Width - seriesPicsWidth) / 2);

            if (textSizeSeries.Width > seriesPicsWidth)
            {
                // error, label to large
                _logger.Log(LogLevel.Critical, "label is to large for column");
            }

            var x = cumulatedX - labelPos;
            return x;
        }

        private static int CountTrue(params bool[] value) => value.Count(v => v);
    }
}