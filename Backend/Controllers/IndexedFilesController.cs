﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Backend.Configurations;
using Backend.Helpers.Sieve;
using Backend.Messages;
using Backend.Repositories;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sieve.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Processing.Processors.Transforms;

namespace Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IndexedFilesController : ControllerBase
    {
        private readonly ILogger<IndexedFilesController> _logger;
        private readonly IIndexedFileRepository _indexedFileRepository;
        private readonly DocMyPicsConfiguration _configuration;

        public IndexedFilesController(ILogger<IndexedFilesController> logger,
            IIndexedFileRepository indexedFileRepository,
            IOptions<DocMyPicsConfiguration> configuration
        )
        {
            _logger = logger;
            _indexedFileRepository = indexedFileRepository;
            _configuration = configuration.Value;
        }

        // GET IndexedFiles
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<IndexedFileResponse>>> GetIndexedFiles(
            [FromQuery] SieveModel sieveModel)
        {
            var result = await _indexedFileRepository.GetIndexedFilesAsync(sieveModel);
            return Ok(result.Adapt<PagedResult<IndexedFileResponse>>());
        }

        [HttpGet("folders")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<FolderNameResponse>>> GetFolderNames([FromQuery] SieveModel sieveModel)
        {
            var result = await _indexedFileRepository.GetIndexedFolderNamesAsync(sieveModel);
            return Ok(result);
        }

        [HttpGet("{id:int}/original")]
        [ResponseCache(Duration = 60 * 60 * 24)]
        public async Task<ActionResult> GetIndexedFileByIdOriginal([FromRoute] int id)
        {
            var indexedFile = await _indexedFileRepository.GetIndexedFileByIdAsync(id);
            if (indexedFile == null)
            {
                return NotFound();
            }

            var path = Path.Combine(_configuration.Folder, indexedFile.Directory == "." ? "" : indexedFile.Directory, indexedFile.FileName);
            var file = System.IO.File.Open(path, FileMode.Open);

            var provider = new FileExtensionContentTypeProvider();
            provider.TryGetContentType(path, out var mime);
            // return Ok(mime);
            return File(file, mime);
        }

        [HttpGet("{id:int}/minimized/{shortestSideLength:int}")]
        [ResponseCache(Duration = 60 * 60 * 24 * 7)]
        public async Task<ActionResult> GetIndexedFileByIdMinimized([FromRoute] int id,
            [FromRoute] int shortestSideLength)
        {
            var indexedFile = await _indexedFileRepository.GetIndexedFileByIdAsync(id);
            if (indexedFile == null)
            {
                return NotFound();
            }

            var path = Path.Combine(_configuration.Folder, indexedFile.Directory == "." ? "" : indexedFile.Directory, indexedFile.FileName);

            Stream outputStream = new MemoryStream();
            var file = new FileStream(path, FileMode.Open);
            var (image, format) = await Image.LoadWithFormatAsync(file);
            file.Close();
            image.Mutate(x =>
                {
                    var newWidth = 0;
                    var newHeight = 0;

                    if (x.GetCurrentSize().Width >= x.GetCurrentSize().Height)
                    {
                        newHeight = shortestSideLength;
                    }
                    else
                    {
                        newWidth = shortestSideLength;
                    }

                    x.Resize(newWidth, newHeight, new NearestNeighborResampler());
                }
            );
            // var provider = new FileExtensionContentTypeProvider();
            // provider.TryGetContentType(path, out var mime);
            await image.SaveAsJpegAsync(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return File(outputStream, format.DefaultMimeType);
        }

        [HttpPatch("{id:int}")]
        [ResponseCache(Duration = 60 * 60 * 24)]
        public async Task<ActionResult> UpdateIndexedFile([FromRoute] int id, [FromBody] UpdateIndexedFile updateIndexedFile)
        {
            await _indexedFileRepository.UpdateIndexedFileAttributesAsync(updateIndexedFile);

            return NoContent();
        }
    }
}