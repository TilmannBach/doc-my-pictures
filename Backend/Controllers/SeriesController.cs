using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Backend.Messages;
using Backend.Repositories;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sieve.Models;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeriesController : ControllerBase
    {
        private readonly ISeriesRepository _seriesRepository;
        private readonly ILogger<SeriesController> _logger;

        public SeriesController(
            ILogger<SeriesController> logger,
            ISeriesRepository seriesRepository
        )
        {
            _logger = logger;
            _seriesRepository = seriesRepository;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<SeriesResponse>>> GetSeries([FromQuery] SieveModel sieveModel)
        {
            var result = await _seriesRepository.GetSeriesAsync(sieveModel);

            return Ok(result.Adapt<PagedResult<SeriesResponse>>());
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<SeriesResponse>> CreateSeriesAsync([FromBody] CreateSeries createSeries)
        {
            var newSeries = createSeries.Adapt<Series>();
            var series = await _seriesRepository.CreateSeriesAsync(newSeries);

            return CreatedAtAction(nameof(GetSeries), new {filters = $"id=={series.Id}"}, series.Adapt<SeriesResponse>());
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GridResponse>> UpdateSeriesAsync([FromBody] UpdateSeries updateSeries, [FromRoute] int id)
        {
            var series = updateSeries.Adapt<Series>();
            await _seriesRepository.UpdateSeriesAsync(series);

            return Ok(series.Adapt<SeriesResponse>());
        }

        [HttpPut("{id:int}/visibility-micro/{isVisible:bool}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GridResponse>> SetSeriesVisibilityMicroAsync([FromRoute] int id, [FromRoute] bool isVisible)
        {
            var series = await _seriesRepository.SetSeriesVisibilityMicroAsync(id, isVisible);

            return Ok(series.Adapt<SeriesResponse>());
        }

        [HttpPut("{id:int}/visibility-macro/{isVisible:bool}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GridResponse>> SetSeriesVisibilityMacroAsync([FromRoute] int id, [FromRoute] bool isVisible)
        {
            var series = await _seriesRepository.SetSeriesVisibilityMacroAsync(id, isVisible);

            return Ok(series.Adapt<SeriesResponse>());
        }
    }
}