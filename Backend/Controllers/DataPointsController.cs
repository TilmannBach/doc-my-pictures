using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Helpers.Sieve;
using Backend.Messages;
using Backend.Repositories;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sieve.Models;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataPointsController : ControllerBase
    {
        private readonly IDataPointRepository _dataPointRepository;
        private readonly ILogger<DataPointsController> _logger;

        public DataPointsController(
            ILogger<DataPointsController> logger,
            IDataPointRepository dataPointRepository
        )
        {
            _logger = logger;
            _dataPointRepository = dataPointRepository;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<DataPointResponse>>> GetDataPoints([FromQuery] SieveModel sieveModel)
        {
            var result = await _dataPointRepository.GetDataPointsAsync(sieveModel);

            return Ok(result.Adapt<PagedResult<DataPointResponse>>());
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<DataPointResponse>> CreateDataPoint([FromBody] CreateDataPoint createDataPoint)
        {
            var newDataPoint = createDataPoint.Adapt<DataPoint>();
            var dataPoint = await _dataPointRepository.CreateDataPointAsync(newDataPoint);

            return CreatedAtAction(nameof(GetDataPoints), new {filters = $"id=={dataPoint.Id}"}, dataPoint.Adapt<DataPointResponse>());
        }

        [HttpPut("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GridResponse>> UpdateDataPointAsync([FromBody] UpdateDataPoint updateDataPoint, [FromRoute] int id)
        {
            var dataPoint = updateDataPoint.Adapt<DataPoint>();
            await _dataPointRepository.UpdateDataPointAsync(dataPoint);

            return Ok(dataPoint.Adapt<DataPointResponse>());
        }

        [HttpPut("{id:int}/visibility/{isVisible:bool}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GridResponse>> SetDataPointVisibilityAsync([FromRoute] int id,[FromRoute] bool isVisible)
        {
            var dp = await _dataPointRepository.SetDataPointVisibilityAsync(id, isVisible);

            return Ok(dp.Adapt<DataPointResponse>());
        }
    }
}