using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Configurations;
using Backend.Contexts;
using Backend.Entities;
using Backend.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Metadata.Profiles.Exif;

namespace Backend.Services
{
    public class FileIndexer : IHostedService
    {
        private readonly ILogger<FileIndexer> _logger;
        private readonly IServiceProvider _services;
        private readonly IOptions<DocMyPicsConfiguration> _config;
        private readonly IWebHostEnvironment _env;

        public FileIndexer(
            ILogger<FileIndexer> logger,
            IServiceProvider services,
            IOptions<DocMyPicsConfiguration> config,
            IWebHostEnvironment env
        )
        {
            _logger = logger;
            _services = services;
            _config = config;
            _env = env;
        }

        private async Task RefreshIndex()
        {
            var files = Directory.EnumerateFiles(_config.Value.Folder, "*.*", SearchOption.AllDirectories)
                .Where(f => f.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) ||
                            f.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase))
                .ToList();
            var currentDir = "";
            var progress = 0.0;
            using var scope = _services.CreateScope();
            var fileRepository = scope.ServiceProvider.GetRequiredService<IIndexedFileRepository>();
            var startTime = DateTime.Now;
            for (var i = 0; i < files.Count; i++)
            {
                var file = Path.GetFileName(files[i]);
                if (currentDir != Path.GetDirectoryName(files[i]))
                {
                    currentDir = Path.GetDirectoryName(files[i]);
                    _logger.LogInformation("Analyzing directory {DirName}", currentDir);
                }

                if ((double) (i + 1) / files.Count * 100 >
                    Math.Ceiling(progress / 10) * 10) // round to nearest times of 10
                {
                    progress = (double) (i + 1) / files.Count * 100;
                    _logger.LogInformation("Current progress {Progress}%", Math.Round(progress, 0));
                }

                var dbDir = currentDir!.Replace(_config.Value.Folder, "").Trim(Path.DirectorySeparatorChar);
                dbDir = dbDir.Length == 0 ? "." : dbDir;

                var dbEntry = await fileRepository.GetIndexedFileByPathAsync(dbDir, file);
                if (dbEntry != null) continue;

                _logger.LogInformation("Found new picture at {Path}", Path.Combine(currentDir ?? string.Empty, file));
                var dateTimeOriginal = await GetDateTimeOriginal(files[i]) ?? GetFallbackFileTimestamp(files[i]);

                await fileRepository.CreateIndexedFileAsync(new IndexedFile
                {
                    Directory = dbDir,
                    FileName = file,
                    DateAdded = DateTime.Now,
                    DateCreated = dateTimeOriginal
                });
            }

            _logger.LogInformation("Analyze time {Duration}", DateTime.Now - startTime);
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (_env.IsProduction())
            {
                using var scope = _services.CreateScope();
                var context = scope.ServiceProvider.GetRequiredService<DocMyPicturesContext>();

                _logger.LogInformation("Starting to migrate the database...");
                await context.Database.MigrateAsync();
            }

            _logger.LogInformation("Starting file indexing...");
            _logger.LogInformation("Index file: {ConfiguredFile}", _config.Value.DbFile);
            await RefreshIndex();
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Task.CompletedTask;
        }

        private async Task<DateTime?> GetDateTimeOriginal(string file)
        {
            var image = await Image.LoadAsync(file);

            if (image.Metadata?.ExifProfile is null)
            {
                return null;
            }

            var dateTimeOriginalString = image.Metadata.ExifProfile.GetValue(ExifTag.DateTimeOriginal);
            image.Dispose();

            if (dateTimeOriginalString.Value == "") return null;

            DateTime.TryParseExact(dateTimeOriginalString.Value, "yyyy:M:dd HH:mm:ss", null,
                DateTimeStyles.AssumeLocal, out var dateTimeOriginal);
            if (dateTimeOriginal == DateTime.MinValue) return null;

            return dateTimeOriginal;
        }

        private DateTime? GetFallbackFileTimestamp(string file)
        {
            var fi = new FileInfo(file);
            return fi.LastWriteTime;
        }
    }
}