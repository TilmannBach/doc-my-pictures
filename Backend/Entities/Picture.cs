using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Backend.Entities
{
    [Table("Pictures")]
    public class Picture
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Id { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public bool IsFavorite { get; set; } = false;

        [Required]
        public int IndexedFileId { get; set; }

        public IndexedFile IndexedFile { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public int Order { get; set; }

        [Required]
        public int GridEntryId { get; set; }

        public GridEntry GridEntry { get; set; }

        public PictureType PictureType { get; set; }
    }
}