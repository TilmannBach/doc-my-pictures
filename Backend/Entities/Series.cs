using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Backend.Entities
{
    [Table("Series")]
    public class Series
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Id { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public string Name { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Order { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public int GridId { get; set; }

        public Grid Grid { get; set; }

        public bool MicroIsVisible { get; set; } = true;
        public bool MacroIsVisible { get; set; } = true;
    }
}