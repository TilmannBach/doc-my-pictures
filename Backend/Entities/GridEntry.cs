using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Backend.Entities
{
    [Table("GridEntries")]
    public class GridEntry
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Id { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public int SeriesId { get; set; }

        public Series Series { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public int DataPointId { get; set; }

        public DataPoint DataPoint { get; set; }

        public IList<Picture> Pictures { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public int GridId { get; set; }

        public Grid Grid { get; set; }
    }
}