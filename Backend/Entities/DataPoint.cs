using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Backend.Entities
{
    [Table("DataPoints")]
    public class DataPoint
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Id { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public string Name { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Order { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public int GridId { get; set; }

        public Grid Grid { get; set; }

        public bool IsVisible { get; set; } = true;
    }
}