using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Backend.Entities
{
    [Table("Grids")]
    public class Grid
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Id { get; set; }

        public IList<Series> Series { get; set; } = new List<Series>();
        public IList<DataPoint> DataPoints { get; set; } = new List<DataPoint>();
        public IList<GridEntry> GridEntries { get; set; } = new List<GridEntry>();

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public string Name { get; set; }
    }
}