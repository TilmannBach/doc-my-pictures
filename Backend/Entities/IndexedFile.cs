using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace Backend.Entities
{
    [Table("IndexedFiles")]
    public class IndexedFile
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public int Id { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public string Directory { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public string FileName { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public bool Hidden { get; set; }

        public IList<Picture> Pictures { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public DateTime DateAdded { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public DateTime? DateCreated { get; set; }
    }
}