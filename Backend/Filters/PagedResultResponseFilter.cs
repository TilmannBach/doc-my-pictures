using System.Net;
using Backend.Helpers.Sieve;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Backend.Filters
{
    public class PagedResultResponseFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.HttpContext.Request.Method != WebRequestMethods.Http.Get ||
                context.Result is not OkObjectResult okObjectResult)
                return;

            var pr = okObjectResult.Value;

            if (!pr.GetType().IsGenericType) return;
            if (pr.GetType().GetGenericTypeDefinition() != typeof(PagedResult<>)) return;

            var rowCount = pr.GetType().GetProperty("RowCount").GetValue(pr, null);
            var currentPage = pr.GetType().GetProperty("CurrentPage").GetValue(pr, null);
            var pageCount = pr.GetType().GetProperty("PageCount").GetValue(pr, null);
            var pageSize = pr.GetType().GetProperty("PageSize").GetValue(pr, null);
            var results = pr.GetType().GetProperty("Results").GetValue(pr, null);

            context.HttpContext.Response.Headers.Add("X-Total-Count", rowCount.ToString());
            context.HttpContext.Response.Headers.Add("X-Current-Page", currentPage.ToString());
            context.HttpContext.Response.Headers.Add("X-Page-Count", pageCount.ToString());
            context.HttpContext.Response.Headers.Add("X-Page-Size", pageSize.ToString());
            okObjectResult.Value = results;
        }
    }
}