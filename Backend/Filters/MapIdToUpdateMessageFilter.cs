using System.Collections.Generic;
using System.Linq;
using Backend.Messages;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Backend.Filters
{
    public class MapIdToUpdateMessageFilter : IActionFilter, IOrderedFilter
    {
        public const int FilterOrder = -2100;

        public MapIdToUpdateMessageFilter()
        {
        }

        public int Order => FilterOrder;

        public bool IsReusable => true;

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var relevantMethods = new List<string>
            {
                HttpMethods.Put,
                HttpMethods.Patch,
            };

            if (relevantMethods.All(method => method != context.HttpContext.Request.Method))
            {
                return;
            }

            if (context.Result != null)
            {
                return;
            }

            if (!context.ActionArguments.TryGetValue("id", out var id))
            {
                return;
            }

            if (id is not int baseId)
            {
                return;
            }

            var baseMessage = (BaseUpdateMessage) context.ActionArguments.SingleOrDefault(kvp => kvp.Value is BaseUpdateMessage).Value;
            if (baseMessage == null)
            {
                return;
            }

            // all the magic is here
            baseMessage.Id = baseId;

            // we need to revalidate afterwards
            var controller = (ControllerBase) context.Controller;
            controller.ControllerContext.ModelState.Clear();
            controller.ObjectValidator.Validate(controller.ControllerContext, null, string.Empty, baseMessage);
        }
    }
}