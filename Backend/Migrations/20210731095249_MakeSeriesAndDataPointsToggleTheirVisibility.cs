﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Backend.Migrations
{
    public partial class MakeSeriesAndDataPointsToggleTheirVisibility : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "MacroIsVisible",
                table: "Series",
                type: "INTEGER",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "MicroIsVisible",
                table: "Series",
                type: "INTEGER",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsVisible",
                table: "DataPoints",
                type: "INTEGER",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MacroIsVisible",
                table: "Series");

            migrationBuilder.DropColumn(
                name: "MicroIsVisible",
                table: "Series");

            migrationBuilder.DropColumn(
                name: "IsVisible",
                table: "DataPoints");
        }
    }
}
