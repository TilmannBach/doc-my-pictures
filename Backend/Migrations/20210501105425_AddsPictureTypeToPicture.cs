﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Backend.Migrations
{
    public partial class AddsPictureTypeToPicture : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Pictures_GridEntryId",
                table: "Pictures");

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Pictures",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PictureType",
                table: "Pictures",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Pictures_GridEntryId",
                table: "Pictures",
                column: "GridEntryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Pictures_GridEntryId",
                table: "Pictures");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Pictures");

            migrationBuilder.DropColumn(
                name: "PictureType",
                table: "Pictures");

            migrationBuilder.CreateIndex(
                name: "IX_Pictures_GridEntryId",
                table: "Pictures",
                column: "GridEntryId",
                unique: true);
        }
    }
}
