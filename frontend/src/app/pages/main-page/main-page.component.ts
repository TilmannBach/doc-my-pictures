import {Component, OnInit} from '@angular/core';
import {TuiDestroyService} from '@taiga-ui/cdk';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {filter, takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.less'],
  providers: [TuiDestroyService],
})
export class MainPageComponent implements OnInit {
  public noGridSelected = true;

  constructor(
    private router: Router,
    private destroy$: TuiDestroyService,
  ) {
  }

  ngOnInit(): void {
    this.router.events.pipe(
      takeUntil(this.destroy$),
      filter((ev: RouterEvent) => ev instanceof NavigationEnd),
      tap(ev => this.noGridSelected = !ev.id),
    ).subscribe();

    this.noGridSelected = this.router.routerState.snapshot.root.firstChild.children.length === 0;
  }
}
