import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GridResponse} from '../../api/models/grid-response';
import {GridsService} from '../../api/services/grids.service';
import {map, shareReplay, switchMap, take, takeUntil} from 'rxjs/operators';
import {TuiDestroyService} from '@taiga-ui/cdk';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {PictureType} from '../../api/models/picture-type';
import {TuiDialogService} from '@taiga-ui/core';
import {SeriesService} from '../../api/services/series.service';
import {DataPointsService} from '../../api/services/data-points.service';
import {PicturesService} from '../../api/services/pictures.service';
import {SeriesResponse} from '../../api/models/series-response';
import {DataPointResponse} from '../../api/models/data-point-response';

@Component({
  selector: 'app-grid-page',
  templateUrl: './grid-page-rendered.component.html',
  styleUrls: ['./grid-page-rendered.component.less'],
  providers: [TuiDestroyService],
})
export class GridPageRenderedComponent implements OnInit {
  currentGrid: Observable<GridResponse>;
  pictureTypes = PictureType;

  private reload$ = new BehaviorSubject(null);

  constructor(
    private route: ActivatedRoute,
    private gridService: GridsService,
    private seriesService: SeriesService,
    private dataPointService: DataPointsService,
    private pictureService: PicturesService,
    private destroy$: TuiDestroyService,
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
  ) {
  }

  ngOnInit(): void {
    this.currentGrid = combineLatest([this.route.paramMap, this.reload$]).pipe(
      takeUntil(this.destroy$),
      map(args => +args[0].get('id')),
      switchMap(id => this.gridService.getGridById(id)),
      shareReplay(1),
    );
  }

  getImgSrc(series: SeriesResponse, dataPoint: DataPointResponse, pictureType: PictureType): Observable<number | null> {
    return this.currentGrid.pipe(
      take(1),
      map(g => g.gridEntries),
      map(ges => {
        const fields = ges.filter(ge => {
          return (ge.series.id === series.id && ge.dataPoint.id === dataPoint.id);
        });
        if (!fields.length) {
          return null;
        }

        if (fields[0].pictures.length === 0) {
          return null;
        }

        const images = fields[0].pictures.filter(p => p.isFavorite && p.pictureType === pictureType);

        if (images.length !== 1) {
          console.warn('There is no (or multiple) favorites marked for ' + series.name + '/' + dataPoint.name + '/' + pictureType);
          return null;
        }

        return images[0].indexedFileId;
      }),
    );
  }
}
