import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridPageRenderedComponent } from './grid-page-rendered.component';

describe('GridPageRenderedComponent', () => {
  let component: GridPageRenderedComponent;
  let fixture: ComponentFixture<GridPageRenderedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridPageRenderedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridPageRenderedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
