import {Component, Inject, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GridResponse} from '../../api/models/grid-response';
import {GridsService} from '../../api/services/grids.service';
import {map, shareReplay, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {TuiDestroyService} from '@taiga-ui/cdk';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {SeriesResponse} from '../../api/models/series-response';
import {DataPointResponse} from '../../api/models/data-point-response';
import {PictureType} from '../../api/models/picture-type';
import {TuiDialogService} from '@taiga-ui/core';
import {PolymorpheusTemplate} from '@tinkoff/ng-polymorpheus';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CreateSeries} from '../../api/models/create-series';
import {SeriesService} from '../../api/services/series.service';
import {CreateDataPoint} from '../../api/models/create-data-point';
import {DataPointsService} from '../../api/services/data-points.service';
import {PicturesService} from '../../api/services/pictures.service';
import {AssignIndexedPicture} from '../../api/models/assign-indexed-picture';
import {DropEvent} from 'angular-draggable-droppable';
import {IndexedFileResponse} from '../../api/models/indexed-file-response';

@Component({
  selector: 'app-grid-page',
  templateUrl: './grid-page.component.html',
  styleUrls: ['./grid-page.component.less'],
  providers: [TuiDestroyService],
})
export class GridPageComponent implements OnInit {
  currentGrid: Observable<GridResponse>;
  pictureTypes = PictureType;
  addSeriesForm = new FormGroup({
    name: new FormControl('', Validators.required),
  });
  addDataPointForm = new FormGroup({
    name: new FormControl('', Validators.required),
  });
  dragOverIsDuplicate = false;

  private reload$ = new BehaviorSubject(null);

  constructor(
    private route: ActivatedRoute,
    private gridService: GridsService,
    private seriesService: SeriesService,
    private dataPointService: DataPointsService,
    private pictureService: PicturesService,
    private destroy$: TuiDestroyService,
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
  ) {
  }

  ngOnInit(): void {
    this.currentGrid = combineLatest([this.route.paramMap, this.reload$]).pipe(
      takeUntil(this.destroy$),
      map(args => +args[0].get('id')),
      switchMap(id => this.gridService.getGridById(id)),
      shareReplay(1),
    );
  }

  dropped(
    dropData,
    grid: GridResponse,
    series: SeriesResponse,
    dataPoint: DataPointResponse,
    pictureType: PictureType,
  ): void {
    const msg: AssignIndexedPicture = {
      dataPointId: dataPoint.id,
      seriesId: series.id,
      indexedFileId: dropData.dropData.id,
      pictureType,
    };
    if (this.dragOverIsDuplicate) {
      return;
    }
    this.pictureService.assignPicture(msg).pipe(
      tap(() => this.reload$.next(null)),
    ).subscribe();
  }

  addSeries(addSeriesDialog: PolymorpheusTemplate<any>): void {
    this.dialogService.open(addSeriesDialog).subscribe();
  }

  addDataPoint(addDataPointDialog: PolymorpheusTemplate<any>): void {
    this.dialogService.open(addDataPointDialog).subscribe();
  }

  saveNewSeries(): void {
    this.currentGrid.pipe(
      take(1),
      switchMap(g => {
        const createMsg: CreateSeries = {
          gridId: g.id,
          name: this.addSeriesForm.get('name').value,
          order: g.series.length,
        };
        return this.seriesService.createSeries(createMsg);
      }),
      tap(() => this.addSeriesForm.reset()),
      tap(() => this.reload$.next(null)),
    ).subscribe();
  }

  saveNewDataPoint(): void {
    this.currentGrid.pipe(
      take(1),
      switchMap(g => {
        const createMsg: CreateDataPoint = {
          gridId: g.id,
          name: this.addDataPointForm.get('name').value,
          order: g.series.length,
        };
        return this.dataPointService.createDataPoint(createMsg);
      }),
      tap(() => this.addDataPointForm.reset()),
      tap(() => this.reload$.next(null)),
    ).subscribe();
  }

  pictureChanged(): void {
    this.reload$.next(null);
  }

  removeDuplicatesCheck(): void {
    this.dragOverIsDuplicate = false;
  }

  checkForDuplicates($event: DropEvent<IndexedFileResponse>, currentGrid: GridResponse, seriesEntry: SeriesResponse, dataPoint: DataPointResponse, pictureType: PictureType): void {
    console.log($event);
    let foundDuplicate = false;
    const indexedFileId = $event.dropData.id;
    const gridEntries = currentGrid.gridEntries.filter(ge => {
      return ge.series.id === seriesEntry.id && ge.dataPoint.id === dataPoint.id;
    });
    if (!gridEntries.length) {
      this.dragOverIsDuplicate = false;
      return;
    }
    gridEntries[0].pictures.forEach(p => {
      if (p.indexedFileId === indexedFileId && p.pictureType === pictureType) {
        foundDuplicate = true;
      }
    });
    this.dragOverIsDuplicate = foundDuplicate;
  }

  hideRow(id: number): void {
    this.dataPointService.setDataPointVisibility(id, false).pipe(
      tap(() => this.reload$.next(null)),
    ).subscribe();
  }

  showRow(id: number): void {
    this.dataPointService.setDataPointVisibility(id, true).pipe(
      tap(() => this.reload$.next(null)),
    ).subscribe();
  }

  hideColumn(id: number, pictureType: string): void {
    if (pictureType === PictureType.Macro) {
      this.seriesService.setSeriesMacroVisibility(id, false).pipe(
        tap(() => this.reload$.next(null)),
      ).subscribe();
    } else {
      this.seriesService.setSeriesMicroVisibility(id, false).pipe(
        tap(() => this.reload$.next(null)),
      ).subscribe();
    }
  }

  showColumn(id: number, pictureType: string): void {
    if (pictureType === PictureType.Macro) {
      this.seriesService.setSeriesMacroVisibility(id, true).pipe(
        tap(() => this.reload$.next(null)),
      ).subscribe();
    } else {
      this.seriesService.setSeriesMicroVisibility(id, true).pipe(
        tap(() => this.reload$.next(null)),
      ).subscribe();
    }
  }
}
