import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridEntryPictureListComponent } from './grid-entry-picture-list.component';

describe('GridEntryPictureListComponent', () => {
  let component: GridEntryPictureListComponent;
  let fixture: ComponentFixture<GridEntryPictureListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridEntryPictureListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridEntryPictureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
