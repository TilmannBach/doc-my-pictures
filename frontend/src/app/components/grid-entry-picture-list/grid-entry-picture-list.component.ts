import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GridEntryResponse} from '../../api/models/grid-entry-response';
import {PictureResponse} from '../../api/models/picture-response';
import {PictureType} from '../../api/models/picture-type';

@Component({
  selector: 'app-grid-entry-picture-list',
  templateUrl: './grid-entry-picture-list.component.html',
  styleUrls: ['./grid-entry-picture-list.component.less'],
})
export class GridEntryPictureListComponent implements OnInit {

  @Input() gridEntries: GridEntryResponse[];
  @Input() seriesId: number;
  @Input() dataPointId: number;
  @Input() pictureType: PictureType;

  @Output() changed = new EventEmitter<void>();

  pictures: PictureResponse[] = [];
  PictureType = PictureType;

  constructor() {
  }

  ngOnInit(): void {
    if (!this.gridEntries) {
      throw new Error('gridEntries property of app-grid-entry-picture-list needs to be set on initialization!');
    }
    if (!this.seriesId) {
      throw new Error('seriesId property of app-grid-entry-picture-list needs to be set on initialization!');
    }
    if (!this.dataPointId) {
      throw new Error('dataPointId property of app-grid-entry-picture-list needs to be set on initialization!');
    }
    if (!this.pictureType) {
      throw new Error('pictureType property of app-grid-entry-picture-list needs to be set on initialization!');
    }

    const suitableEntries = this.gridEntries.filter(value => {
      return value.series.id === this.seriesId && value.dataPoint.id === this.dataPointId;
    });

    if (suitableEntries.length === 0) {
      return;
    }

    this.pictures = suitableEntries[0].pictures.filter(p => p.pictureType === this.pictureType);
  }

  pictureChanged(pic: PictureResponse): void {
    this.changed.emit();
  }
}
