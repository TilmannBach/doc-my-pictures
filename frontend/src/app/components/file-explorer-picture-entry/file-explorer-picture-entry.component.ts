import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IndexedFileResponse} from '../../api/models/indexed-file-response';

@Component({
  selector: 'app-file-explorer-picture-entry',
  templateUrl: './file-explorer-picture-entry.component.html',
  styleUrls: ['./file-explorer-picture-entry.component.less'],
})
export class FileExplorerPictureEntryComponent implements OnInit {

  @Input() fileEntry: IndexedFileResponse;
  @Output() hiddenToggled = new EventEmitter<IndexedFileResponse>();

  isDragging = false;

  constructor() {
  }

  ngOnInit(): void {
    if (!this.fileEntry) {
      throw new Error('fileEntry property of app-file-explorer-picture-entry needs to be set on initialization!');
    }
  }

  toggleHidden(fileEntry: IndexedFileResponse): void {
    this.hiddenToggled.emit(fileEntry);
  }
}
