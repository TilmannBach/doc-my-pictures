import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileExplorerPictureEntryComponent } from './file-explorer-picture-entry.component';

describe('FileExplorerPictureEntryComponent', () => {
  let component: FileExplorerPictureEntryComponent;
  let fixture: ComponentFixture<FileExplorerPictureEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileExplorerPictureEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileExplorerPictureEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
