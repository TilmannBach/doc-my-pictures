import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IndexedFileResponse} from '../../api/models/indexed-file-response';
import {IndexedFilesService} from '../../api/services/indexed-files.service';
import {Filter} from '../../api/filter/filter';
import {IndexedFilesFilter} from '../../api/services/indexed-files-filter';
import {RelationalOperator} from '../../api/filter/relational-operator';
import {SortDirection} from '../../api/filter/sort-direction';
import {debounceTime, switchMap, takeUntil, tap} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {TuiDestroyService} from '@taiga-ui/cdk';
import {ConfigurationService} from '../../services/configuration.service';

@Component({
  selector: 'app-file-explorer-picture-view',
  templateUrl: './file-explorer-picture-view.component.html',
  styleUrls: ['./file-explorer-picture-view.component.less'],
  providers: [TuiDestroyService],
})
export class FileExplorerPictureViewComponent implements OnInit {
  fileList: Observable<IndexedFileResponse[]>;

  usagesComparators = [
    {backend: '==', frontend: 'Exact'},
    {backend: '>', frontend: 'More than'},
    {backend: '<', frontend: 'Less than'},
  ];

  @Input() folder: string;

  @Output() back = new EventEmitter<void>();

  filterForm = new FormGroup({
    name: new FormControl(''),
    sort: new FormControl(null),
    showHidden: new FormControl(null),
    usagesComparator: new FormControl(null),
    usages: new FormGroup({
      comparator: new FormControl(null),
      count: new FormControl(null),
    }),
  });

  sorts = [
    {name: 'File name (asc)', value: 'fileName', order: SortDirection.ASC},
    {name: 'File name (desc)', value: 'fileName', order: SortDirection.DESC},
    {name: 'Date created (asc)', value: 'dateCreated', order: SortDirection.ASC},
    {name: 'Date created (desc)', value: 'dateCreated', order: SortDirection.DESC},
  ];

  filter: Filter<IndexedFilesFilter>;

  private reload$ = new BehaviorSubject(null);

  constructor(
    private destroy$: TuiDestroyService,
    private indexedFiles: IndexedFilesService,
    private configuration: ConfigurationService,
  ) {
  }

  ngOnInit(): void {
    if (this.folder === null) {
      throw new Error('folder property of app-file-explorer-picture-view needs to be set on initialization!');
    }

    this.filterForm.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(300),
      tap(val => this.filter.clearFilter('fileName')),
      tap(val => this.filter.clearSort()),
      tap(val => this.filter.clearFilter('hidden')),
      tap(val => this.filter.clearFilter('usages')),
      tap(val => {
        if (val.name) {
          this.filter.setFilter('fileName', RelationalOperator.CONTAINS_CI, val.name);
        }
        if (val.sort) {
          this.filter.setSort(val.sort.value, val.sort.order);
          this.configuration.setSortValue(val.sort.value);
          this.configuration.setSortDirection(val.sort.order);
        }
        if (!val.showHidden) {
          this.filter.setFilter('hidden', RelationalOperator.EQUALS, false);
        }
        if (val.usages.comparator && val.usages.count !== null) {
          this.filter.setFilter('usages', val.usages.comparator.backend, val.usages.count);
        }
      }),
      tap(() => this.reload$.next(null)),
    ).subscribe();

    this.filter = new Filter(IndexedFilesFilter);
    this.filter.setFilter('hidden', RelationalOperator.EQUALS, false);
    this.filter.setFilter('directory', RelationalOperator.EQUALS_CI, encodeURIComponent(this.folder));

    if (this.configuration.getSortValue()) {
      this.sorts.forEach(sv => {
        if (sv.value === this.configuration.getSortValue() && sv.order === this.configuration.getSortDirection()) {
          this.filterForm.get('sort').patchValue(sv);
        }
      });
    }

    this.fileList = this.reload$.pipe(
      takeUntil(this.destroy$),
      switchMap(() => this.indexedFiles.getAllIndexedFilesFiltered(this.filter)),
    );
  }

  goBack(): void {
    this.back.emit();
  }

  togglePictureHidden(file: IndexedFileResponse): void {
    this.indexedFiles.setIndexedFileHidden(file.id, !file.hidden).pipe(
      tap(() => this.reload$.next(null)),
    ).subscribe();
  }
}
