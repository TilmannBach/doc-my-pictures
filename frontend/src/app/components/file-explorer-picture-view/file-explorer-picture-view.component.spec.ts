import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileExplorerPictureViewComponent } from './file-explorer-picture-view.component';

describe('FileExplorerPictureViewComponent', () => {
  let component: FileExplorerPictureViewComponent;
  let fixture: ComponentFixture<FileExplorerPictureViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileExplorerPictureViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileExplorerPictureViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
