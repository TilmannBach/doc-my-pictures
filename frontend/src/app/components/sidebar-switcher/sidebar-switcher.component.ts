import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {takeUntil, tap} from 'rxjs/operators';
import {TuiDestroyService} from '@taiga-ui/cdk';
import {ConfigurationService} from '../../services/configuration.service';

@Component({
  selector: 'app-sidebar-switcher',
  templateUrl: './sidebar-switcher.component.html',
  styleUrls: ['./sidebar-switcher.component.less'],
  providers: [TuiDestroyService],
})
export class SidebarSwitcherComponent implements OnInit {
  testForm = new FormGroup({
    enableExplorer: new FormControl(false),
    enableGridManager: new FormControl(false),
  });

  explorerOpened = false;
  gridManagerOpened = false;
  anyOpened = false;

  constructor(private destroy$: TuiDestroyService, private configuration: ConfigurationService) {
  }

  ngOnInit(): void {
    this.explorerOpened = this.configuration.getFileExplorer();
    this.testForm.get('enableExplorer').patchValue(this.explorerOpened);
    this.gridManagerOpened = this.configuration.getGridManager();
    this.testForm.get('enableGridManager').patchValue(this.gridManagerOpened);
    this.anyOpened = this.gridManagerOpened || this.explorerOpened;

    this.testForm.get('enableExplorer').valueChanges.pipe(
      takeUntil(this.destroy$),
      tap(isOpen => this.explorerOpened = isOpen),
      tap(isOpen => this.configuration.setFileExplorer(isOpen)),
      tap(isOpen => {
        if (isOpen && this.gridManagerOpened) {
          this.testForm.get('enableGridManager').patchValue(false);
        }
      }),
      tap(() => this.anyOpened = this.gridManagerOpened || this.explorerOpened),
    ).subscribe();
    this.testForm.get('enableGridManager').valueChanges.pipe(
      takeUntil(this.destroy$),
      tap(isOpen => this.gridManagerOpened = isOpen),
      tap(isOpen => this.configuration.setGridManager(isOpen)),
      tap(isOpen => {
        if (isOpen && this.explorerOpened) {
          this.testForm.get('enableExplorer').patchValue(false);
        }
      }),
      tap(() => this.anyOpened = this.gridManagerOpened || this.explorerOpened),
    ).subscribe();
  }

}
