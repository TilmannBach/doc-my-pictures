import {Component, OnInit} from '@angular/core';
import {IndexedFilesService} from '../../api/services/indexed-files.service';
import {ConfigurationService} from '../../services/configuration.service';

@Component({
  selector: 'app-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.less'],
})
export class FileExplorerComponent implements OnInit {

  currentFolder: string | null;

  constructor(
    private indexedFiles: IndexedFilesService,
    private configService: ConfigurationService,
  ) {
  }

  ngOnInit(): void {
    this.currentFolder = this.configService.getCurrentFolder();
  }

  setCurrentFolder($event: string | null): void {
    this.configService.setCurrentFolder($event);
    this.currentFolder = $event;
  }
}
