import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {FolderNameResponse} from '../../api/models/folder-name-response';
import {IndexedFilesService} from '../../api/services/indexed-files.service';
import {debounceTime, switchMap, takeUntil, tap} from 'rxjs/operators';
import {TuiDestroyService} from '@taiga-ui/cdk';

@Component({
  selector: 'app-file-explorer-folder-view',
  templateUrl: './file-explorer-folder-view.component.html',
  styleUrls: ['./file-explorer-folder-view.component.less'],
  providers: [TuiDestroyService],
})
export class FileExplorerFolderViewComponent implements OnInit {

  @Output() folder = new EventEmitter<string>();

  filterForm = new FormGroup({
    name: new FormControl(''),
  });

  filter = '';
  folders: Observable<FolderNameResponse[]>;

  private reload$ = new BehaviorSubject(null);

  constructor(
    private destroy$: TuiDestroyService,
    private indexedFilesServide: IndexedFilesService,
  ) {
  }

  ngOnInit(): void {
    this.filter = this.filterForm.get('name').value;

    this.folders = this.reload$.asObservable().pipe(
      switchMap(() => this.indexedFilesServide.getAllFolders(this.filter)),
    );

    this.filterForm.get('name').valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(300),
      tap(val => this.filter = val),
      tap(() => this.reload$.next(null)),
    ).subscribe();
  }


  selectFolder(folder: FolderNameResponse): void {
    console.log('folder selected:');
    console.log(folder);
    this.folder.emit(folder.directory);
  }
}
