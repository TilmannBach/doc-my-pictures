import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileExplorerFolderViewComponent } from './file-explorer-folder-view.component';

describe('FileExplorerFolderViewComponent', () => {
  let component: FileExplorerFolderViewComponent;
  let fixture: ComponentFixture<FileExplorerFolderViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileExplorerFolderViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileExplorerFolderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
