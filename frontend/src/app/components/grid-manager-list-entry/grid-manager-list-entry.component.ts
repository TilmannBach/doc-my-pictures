import {Component, Input, OnInit} from '@angular/core';
import {GridResponse} from '../../api/models/grid-response';

@Component({
  selector: 'app-grid-manager-list-entry',
  templateUrl: './grid-manager-list-entry.component.html',
  styleUrls: ['./grid-manager-list-entry.component.less'],
})
export class GridManagerListEntryComponent implements OnInit {

  @Input() grid: GridResponse;

  constructor() {
  }

  ngOnInit(): void {
    if (!this.grid) {
      throw new Error('grid property of app-grid-manager-list-entry needs to be set on initialization!');
    }
  }

  _(): void {
    return;
  }
}
