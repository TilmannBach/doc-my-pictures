import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridManagerListEntryComponent } from './grid-manager-list-entry.component';

describe('GridManagerListEntryComponent', () => {
  let component: GridManagerListEntryComponent;
  let fixture: ComponentFixture<GridManagerListEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridManagerListEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridManagerListEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
