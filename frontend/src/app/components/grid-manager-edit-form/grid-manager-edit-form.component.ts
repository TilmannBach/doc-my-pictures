import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {GridResponse} from '../../api/models/grid-response';
import {GridsService} from '../../api/services/grids.service';
import {UpdateGrid} from '../../api/models/update-grid';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-grid-manager-edit-form',
  templateUrl: './grid-manager-edit-form.component.html',
  styleUrls: ['./grid-manager-edit-form.component.less'],
})
export class GridManagerEditFormComponent implements OnInit {

  @Input() grid: GridResponse;
  @Output() saved = new EventEmitter<GridResponse>();

  editForm = new FormGroup({
    name: new FormControl(''),
  });

  constructor(
    private gridService: GridsService,
  ) {
  }

  ngOnInit(): void {
    if (!this.grid) {
      throw new Error('grid property of app-grid-manager-edit-form needs to be set on initialization!');
    }
    this.editForm.patchValue(this.grid);
  }

  saveForm(): void {
    const updateMsg: UpdateGrid = {
      name: this.editForm.get('name').value,
    };
    this.gridService.updateGrid(this.grid.id, updateMsg).pipe(
      tap(grid => this.saved.emit(grid)),
    ).subscribe();
  }
}
