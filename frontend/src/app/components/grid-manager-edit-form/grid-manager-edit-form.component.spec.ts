import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridManagerEditFormComponent } from './grid-manager-edit-form.component';

describe('GridManagerEditFormComponent', () => {
  let component: GridManagerEditFormComponent;
  let fixture: ComponentFixture<GridManagerEditFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridManagerEditFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridManagerEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
