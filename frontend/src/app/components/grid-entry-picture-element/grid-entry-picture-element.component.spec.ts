import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridEntryPictureElementComponent } from './grid-entry-picture-element.component';

describe('GridEntryPictureElementComponent', () => {
  let component: GridEntryPictureElementComponent;
  let fixture: ComponentFixture<GridEntryPictureElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridEntryPictureElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridEntryPictureElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
