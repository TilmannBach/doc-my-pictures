import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {PictureResponse} from '../../api/models/picture-response';
import {PicturesService} from '../../api/services/pictures.service';
import {tap} from 'rxjs/operators';
import {PictureType} from '../../api/models/picture-type';
import {TuiDialogContext, TuiDialogService} from '@taiga-ui/core';
import {PolymorpheusContent, PolymorpheusTemplate} from '@tinkoff/ng-polymorpheus';

@Component({
  selector: 'app-grid-entry-picture-element',
  templateUrl: './grid-entry-picture-element.component.html',
  styleUrls: ['./grid-entry-picture-element.component.less'],
})
export class GridEntryPictureElementComponent implements OnInit {

  @Input() picture: PictureResponse;

  @Output() changed = new EventEmitter<void>();

  open = false;
  PictureType = PictureType;

  constructor(
    @Inject(TuiDialogService) private readonly dialogService: TuiDialogService,
    private pictureService: PicturesService,
  ) {
  }

  ngOnInit(): void {
    if (!this.picture) {
      throw new Error('picture property of app-grid-entry-picture-element needs to be set on initialization!');
    }

  }

  onClick($event): void {
    console.log($event);
  }

  toggleFav(picture: PictureResponse): void {
    if (picture.isFavorite) {
      this.pictureService.markPictureAsNonFavorite(picture.id).pipe(
        tap(() => this.changed.emit()),
      ).subscribe();
    } else {
      this.pictureService.markPictureAsFavorite(picture.id).pipe(
        tap(() => this.changed.emit()),
      ).subscribe();
    }
  }

  togglePictureType(picture: PictureResponse): void {
    const newType = picture.pictureType === PictureType.Micro ? PictureType.Macro : PictureType.Micro;
    this.pictureService.setPictureType(picture.id, newType).pipe(
      tap(() => this.changed.emit()),
    ).subscribe();
  }

  showDeleteDialog(template: PolymorpheusTemplate<TuiDialogContext>, header: PolymorpheusContent): void {
    this.dialogService.open(template, {header}).subscribe();
  }

  deletePicture(picture: PictureResponse, observer): void {
    this.pictureService.deletePicture(picture.id).pipe(
      tap(() => this.changed.emit()),
      tap(() => observer.complete()),
    ).subscribe();
  }
}
