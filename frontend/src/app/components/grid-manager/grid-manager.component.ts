import {Component, OnInit} from '@angular/core';
import {GridsService} from '../../api/services/grids.service';
import {Filter} from '../../api/filter/filter';
import {GridFilter} from '../../api/services/grid-filter';
import {Observable} from 'rxjs';
import {GridResponse} from '../../api/models/grid-response';
import {PagedResult} from '../../api/paged-result';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime, takeUntil, tap} from 'rxjs/operators';
import {TuiDestroyService} from '@taiga-ui/cdk';
import {RelationalOperator} from '../../api/filter/relational-operator';
import {CreateGrid} from '../../api/models/create-grid';
import {SortDirection} from '../../api/filter/sort-direction';

@Component({
  selector: 'app-grid-manager',
  templateUrl: './grid-manager.component.html',
  styleUrls: ['./grid-manager.component.less'],
  providers: [TuiDestroyService],
})
export class GridManagerComponent implements OnInit {
  gridsResponse: Observable<PagedResult<GridResponse>>;

  private filter = (new Filter(GridFilter)).setSort('name', SortDirection.ASC);
  filterForm = new FormGroup({
    name: new FormControl(''),
  });
  createGridForm = new FormGroup({
    name: new FormControl('', Validators.required),
  });
  showCreateForm = false;

  constructor(
    private gridService: GridsService,
    private destroy$: TuiDestroyService,
  ) {
  }

  ngOnInit(): void {
    this.loadFiltered();
    this.filterForm.get('name').valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(300),
      tap(filter => {
        this.filter.clearFilter('name');
        if (filter !== '') {
          this.filter.setFilter('name', RelationalOperator.CONTAINS_CI, filter);
        }
      }),
      tap(() => this.loadFiltered()),
    ).subscribe();
  }

  goToPage($event: number): void {
    this.filter.setPage($event);
    this.loadFiltered();
  }

  private loadFiltered(): void {
    this.filter.setPageSize(10);
    this.gridsResponse = this.gridService.getFilteredGrids(this.filter);
  }

  createNewGrid(): void {
    this.createGridForm.reset({name: ''});
    this.showCreateForm = true;
  }

  cancelGridCreation(): void {
    this.showCreateForm = false;
  }

  saveNewGrid(): void {
    const msg: CreateGrid = {
      name: this.createGridForm.get('name').value,
    };
    this.gridService.createGrid(msg).pipe(
      tap(() => {
        this.showCreateForm = false;
        this.filterForm.get('name').patchValue(msg.name);
        this.loadFiltered();
      }),
    ).subscribe();
  }
}
