import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {
    TuiButtonModule,
    TuiDataListModule,
    TuiDialogModule, TuiGroupModule,
    TuiHintControllerModule,
    TuiHostedDropdownModule,
    TuiRootModule,
    TuiScrollbarModule,
    TuiSvgModule,
    TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import {MainPageComponent} from './pages/main-page/main-page.component';
import {
  TuiAccordionModule,
  TuiBadgeModule,
  TuiDataListWrapperModule,
  TuiDropdownHoverModule,
  TuiFieldErrorModule,
  TuiInputModule,
  TuiIslandModule,
  TuiMarkerIconModule,
  TuiPaginationModule,
  TuiSelectModule,
  TuiToggleModule,
} from '@taiga-ui/kit';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FileExplorerComponent} from './components/file-explorer/file-explorer.component';
import {DragAndDropModule} from 'angular-draggable-droppable';
import {SidebarSwitcherComponent} from './components/sidebar-switcher/sidebar-switcher.component';
import {GridManagerComponent} from './components/grid-manager/grid-manager.component';
import {GridManagerEditFormComponent} from './components/grid-manager-edit-form/grid-manager-edit-form.component';
import {GridManagerListEntryComponent} from './components/grid-manager-list-entry/grid-manager-list-entry.component';
import {GridPageComponent} from './pages/grid-page/grid-page.component';
import {PolymorpheusModule} from '@tinkoff/ng-polymorpheus';
import {TuiAutoFocusModule, TuiFocusableModule} from '@taiga-ui/cdk';
import {GridEntryPictureListComponent} from './components/grid-entry-picture-list/grid-entry-picture-list.component';
import {FileExplorerFolderViewComponent} from './components/file-explorer-folder-view/file-explorer-folder-view.component';
import {FileExplorerPictureViewComponent} from './components/file-explorer-picture-view/file-explorer-picture-view.component';
import {GridEntryPictureElementComponent} from './components/grid-entry-picture-element/grid-entry-picture-element.component';
import {FileExplorerPictureEntryComponent} from './components/file-explorer-picture-entry/file-explorer-picture-entry.component';
import {GridPageRenderedComponent} from './pages/grid-page-rendered/grid-page-rendered.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    FileExplorerComponent,
    SidebarSwitcherComponent,
    GridManagerComponent,
    GridManagerEditFormComponent,
    GridManagerListEntryComponent,
    GridPageComponent,
    GridPageRenderedComponent,
    GridEntryPictureListComponent,
    FileExplorerFolderViewComponent,
    FileExplorerPictureViewComponent,
    GridEntryPictureElementComponent,
    FileExplorerPictureEntryComponent,
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        DragAndDropModule,
        PolymorpheusModule,
        TuiRootModule,
        TuiDialogModule,
        TuiIslandModule,
        TuiSvgModule,
        TuiMarkerIconModule,
        TuiInputModule,
        TuiFieldErrorModule,
        TuiTextfieldControllerModule,
        TuiHintControllerModule,
        TuiSelectModule,
        TuiDataListWrapperModule,
        TuiToggleModule,
        TuiHostedDropdownModule,
        TuiDropdownHoverModule,
        TuiDataListModule,
        TuiButtonModule,
        TuiPaginationModule,
        TuiAccordionModule,
        TuiAutoFocusModule,
        TuiScrollbarModule,
        TuiBadgeModule,
        TuiFocusableModule,
        TuiGroupModule,
    ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
