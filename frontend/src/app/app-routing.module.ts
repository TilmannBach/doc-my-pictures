import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainPageComponent} from './pages/main-page/main-page.component';
import {GridPageComponent} from './pages/grid-page/grid-page.component';
import {GridPageRenderedComponent} from './pages/grid-page-rendered/grid-page-rendered.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent,
    children: [
      {
        path: 'grids/:id',
        component: GridPageComponent,
      },
      {
        path: 'grids-final/:id',
        component: GridPageRenderedComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
