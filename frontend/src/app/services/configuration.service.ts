import {Injectable} from '@angular/core';
import {SortDirection} from '../api/filter/sort-direction';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {

  private fileExplorer = false;
  private gridManager = false;
  private currentFolder: string | null = null;
  private sortValue: string | null = null;
  private sortDirection: SortDirection | null = null;

  constructor() {
    this.setFileExplorer(localStorage.getItem('fileExplorer') === 'true');
    this.setGridManager(localStorage.getItem('gridManager') === 'true');
    this.setCurrentFolder(localStorage.getItem('currentFolder'));
    this.setSortValue(localStorage.getItem('sortValue'));
    this.setSortDirection(localStorage.getItem('sortDirection') as SortDirection);
  }

  setFileExplorer(enabled: boolean): void {
    this.fileExplorer = enabled;
    localStorage.setItem('fileExplorer', String(this.fileExplorer));
  }

  setGridManager(enabled: boolean): void {
    this.gridManager = enabled;
    localStorage.setItem('gridManager', String(this.gridManager));
  }

  setCurrentFolder(folder: string | null): void {
    this.currentFolder = folder;
    if (folder === null) {
      localStorage.removeItem('currentFolder');
    } else {
      localStorage.setItem('currentFolder', this.currentFolder);
    }
  }

  setSortValue(sortValue: string | null): void {
    this.sortValue = sortValue;
    if (sortValue === null) {
      localStorage.removeItem('sortValue');
    } else {
      localStorage.setItem('sortValue', this.sortValue);
    }
  }

  setSortDirection(sortDirection: SortDirection | null): void {
    this.sortDirection = sortDirection;
    if (sortDirection === null) {
      localStorage.removeItem('sortDirection');
    } else {
      localStorage.setItem('sortDirection', this.sortDirection);
    }
  }

  getFileExplorer(): boolean {
    return this.fileExplorer;
  }

  getGridManager(): boolean {
    return this.gridManager;
  }

  getCurrentFolder(): string | null {
    return this.currentFolder;
  }

  getSortDirection(): SortDirection | null {
    return this.sortDirection;
  }

  getSortValue(): string | null {
    return this.sortValue;
  }
}
