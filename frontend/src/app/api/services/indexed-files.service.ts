import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {AbstractApiService} from '../abstract-api.service';
import {Filter} from '../filter/filter';
import {IndexedFileResponse} from '../models/indexed-file-response';
import {IndexedFilesFilter} from './indexed-files-filter';
import {PagedResult} from '../paged-result';
import {concatMap, expand, toArray} from 'rxjs/operators';
import {FolderNameResponse} from '../models/folder-name-response';
import {UpdateIndexedFile} from '../models/update-indexed-file';

@Injectable({
  providedIn: 'root',
})
export class IndexedFilesService extends AbstractApiService<IndexedFileResponse> {
  baseUrl = '/api/IndexedFiles';

  constructor(public http: HttpClient) {
    super();
  }

  public fetch = (filter: Filter<IndexedFilesFilter>) => this.getFilteredIndexedFiles(filter);

  getFilteredIndexedFiles(filter: Filter<IndexedFilesFilter>): Observable<PagedResult<IndexedFileResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  getAllFolders(directoryFilter: string = ''): Observable<FolderNameResponse[]> {
    let filter = directoryFilter ? '?filters=directory@=*' + encodeURIComponent(directoryFilter) : '';
    filter = (filter ? filter + '&' : '?') + 'sorts=-directory';
    return this.http.get<FolderNameResponse[]>(this.baseUrl + '/folders' + filter);
  }

  getAllIndexedFilesFiltered(filter: Filter<IndexedFilesFilter>): Observable<IndexedFileResponse[]> {
    filter.setPage(1);
    return this.getFilteredIndexedFiles(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredIndexedFiles(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }

  setIndexedFileHidden(id: number, isHidden: boolean): Observable<void> {
    const msg: UpdateIndexedFile = {
      hidden: isHidden,
    };
    return this.patchGeneric<UpdateIndexedFile, void>(this.baseUrl + '/' + id, msg);
  }

}
