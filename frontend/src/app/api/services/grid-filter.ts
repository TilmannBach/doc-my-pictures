import {RelationalOperator} from '../filter/relational-operator';
import {AbstractFilter} from '../filter/abstract-filter';

export class GridFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public name: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
}
