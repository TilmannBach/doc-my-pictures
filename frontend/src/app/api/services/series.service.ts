import {Injectable} from '@angular/core';
import {AbstractApiService} from '../abstract-api.service';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {Filter} from '../filter/filter';
import {RelationalOperator} from '../filter/relational-operator';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {PagedResult} from '../paged-result';
import {CreateGrid} from '../models/create-grid';
import {SeriesResponse} from '../models/series-response';
import {SeriesFilter} from './series-filter';
import {UpdateSeries} from '../models/update-series';
import {DataPointResponse} from '../models/data-point-response';

@Injectable({
  providedIn: 'root',
})
export class SeriesService extends AbstractApiService<SeriesResponse> {
  baseUrl = '/api/Series';

  constructor(public http: HttpClient) {
    super();
  }

  getSeriesById(id: number): Observable<SeriesResponse | null> {
    const filter = new Filter(SeriesFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('Series with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift()),
    );
  }

  public fetch = (filter: Filter<SeriesFilter>) => this.getFilteredSeries(filter);

  getFilteredSeries(filter: Filter<SeriesFilter>): Observable<PagedResult<SeriesResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  createSeries(createMessage: CreateGrid): Observable<SeriesResponse> {
    return this.postGeneric<CreateGrid, SeriesResponse>(this.baseUrl, createMessage);
  }

  updateSeries(seriesId: number, updateMessage: UpdateSeries): Observable<SeriesResponse> {
    return this.putGeneric<UpdateSeries, SeriesResponse>(this.baseUrl + '/' + seriesId, updateMessage);
  }

  setSeriesMicroVisibility(seriesId: number, microIsVisible): Observable<DataPointResponse> {
    return this.putGeneric<void, DataPointResponse>(this.baseUrl + '/' + seriesId + '/visibility-micro/' + microIsVisible, null);
  }

  setSeriesMacroVisibility(seriesId: number, macroIsVisible): Observable<DataPointResponse> {
    return this.putGeneric<void, DataPointResponse>(this.baseUrl + '/' + seriesId + '/visibility-macro/' + macroIsVisible, null);
  }

  getAllSeriesFiltered(filter: Filter<SeriesFilter>): Observable<SeriesResponse[]> {
    filter.setPage(1);
    return this.getFilteredSeries(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredSeries(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }
}
