import {Injectable} from '@angular/core';
import {AbstractApiService} from '../abstract-api.service';
import {GridResponse} from '../models/grid-response';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {GridFilter} from './grid-filter';
import {Filter} from '../filter/filter';
import {RelationalOperator} from '../filter/relational-operator';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {PagedResult} from '../paged-result';
import {CreateGrid} from '../models/create-grid';
import {UpdateGrid} from '../models/update-grid';

@Injectable({
  providedIn: 'root',
})
export class GridsService extends AbstractApiService<GridResponse> {
  baseUrl = '/api/Grids';

  constructor(public http: HttpClient) {
    super();
  }

  getGridById(id: number): Observable<GridResponse | null> {
    const filter = new Filter(GridFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('Grid with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift()),
    );
  }

  public fetch = (filter: Filter<GridFilter>) => this.getFilteredGrids(filter);

  getFilteredGrids(filter: Filter<GridFilter>): Observable<PagedResult<GridResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  createGrid(createMessage: CreateGrid): Observable<GridResponse> {
    return this.postGeneric<CreateGrid, GridResponse>(this.baseUrl, createMessage);
  }

  updateGrid(gridId: number, updateMessage: UpdateGrid): Observable<GridResponse> {
    return this.putGeneric<UpdateGrid, GridResponse>(this.baseUrl + '/' + gridId, updateMessage);
  }

  getAllGridsFiltered(filter: Filter<GridFilter>): Observable<GridResponse[]> {
    filter.setPage(1);
    return this.getFilteredGrids(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredGrids(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }
}
