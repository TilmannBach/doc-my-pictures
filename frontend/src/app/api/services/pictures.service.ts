import {Injectable} from '@angular/core';
import {AbstractApiService} from '../abstract-api.service';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {Filter} from '../filter/filter';
import {RelationalOperator} from '../filter/relational-operator';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {PagedResult} from '../paged-result';
import {SeriesResponse} from '../models/series-response';
import {SeriesFilter} from './series-filter';
import {PictureResponse} from '../models/picture-response';
import {AssignIndexedPicture} from '../models/assign-indexed-picture';
import {PictureType} from '../models/picture-type';

@Injectable({
  providedIn: 'root',
})
export class PicturesService extends AbstractApiService<PictureResponse> {
  baseUrl = '/api/Pictures';

  constructor(public http: HttpClient) {
    super();
  }

  getPicturesById(id: number): Observable<PictureResponse | null> {
    const filter = new Filter(SeriesFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('Series with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift()),
    );
  }

  public fetch = (filter: Filter<SeriesFilter>) => this.getFilteredPictures(filter);

  getFilteredPictures(filter: Filter<SeriesFilter>): Observable<PagedResult<PictureResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  assignPicture(assignMessage: AssignIndexedPicture): Observable<PictureResponse> {
    return this.postGeneric<AssignIndexedPicture, PictureResponse>(this.baseUrl + '/assign', assignMessage);
  }

  markPictureAsFavorite(pictureId: number): Observable<void> {
    return this.putGeneric<void, void>(this.baseUrl + '/' + pictureId + '/favorite', null);
  }

  markPictureAsNonFavorite(pictureId: number): Observable<void> {
    return this.deleteGeneric<void>(this.baseUrl + '/' + pictureId + '/favorite');
  }

  setPictureType(pictureId: number, type: PictureType): Observable<void> {
    return this.putGeneric<void, void>(this.baseUrl + '/' + pictureId + '/type/' + type, null);
  }

  deletePicture(pictureId: number): Observable<void> {
    return this.deleteGeneric<void>(this.baseUrl + '/' + pictureId);
  }

  getAllSeriesFiltered(filter: Filter<SeriesFilter>): Observable<PictureResponse[]> {
    filter.setPage(1);
    return this.getFilteredPictures(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredPictures(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }
}
