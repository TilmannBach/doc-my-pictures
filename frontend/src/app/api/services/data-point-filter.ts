import {RelationalOperator} from '../filter/relational-operator';
import {AbstractFilter} from '../filter/abstract-filter';

export class DataPointFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public name: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public order: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public gridId: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
}
