import {RelationalOperator} from '../filter/relational-operator';
import {AbstractFilter} from '../filter/abstract-filter';

export class IndexedFilesFilter extends AbstractFilter {
  public id: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
  public directory: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public fileName: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public hidden: Map<RelationalOperator, boolean> = new Map<RelationalOperator, boolean>();
  public dateAdded: Map<RelationalOperator, Date> = new Map<RelationalOperator, Date>();
  public dateCreated: Map<RelationalOperator, Date> = new Map<RelationalOperator, Date>();
  public usages: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
}
