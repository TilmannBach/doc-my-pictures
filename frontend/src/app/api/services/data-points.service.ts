import {Injectable} from '@angular/core';
import {AbstractApiService} from '../abstract-api.service';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {Filter} from '../filter/filter';
import {RelationalOperator} from '../filter/relational-operator';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {PagedResult} from '../paged-result';
import {DataPointResponse} from '../models/data-point-response';
import {CreateDataPoint} from '../models/create-data-point';
import {UpdateDataPoint} from '../models/update-data-point';
import {DataPointFilter} from './data-point-filter';

@Injectable({
  providedIn: 'root',
})
export class DataPointsService extends AbstractApiService<DataPointResponse> {
  baseUrl = '/api/DataPoints';

  constructor(public http: HttpClient) {
    super();
  }

  getDataPointById(id: number): Observable<DataPointResponse | null> {
    const filter = new Filter(DataPointFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('Data-Point with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift()),
    );
  }

  public fetch = (filter: Filter<DataPointFilter>) => this.getFilteredDataPoints(filter);

  getFilteredDataPoints(filter: Filter<DataPointFilter>): Observable<PagedResult<DataPointResponse>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  createDataPoint(createMessage: CreateDataPoint): Observable<DataPointResponse> {
    return this.postGeneric<CreateDataPoint, DataPointResponse>(this.baseUrl, createMessage);
  }

  updateDataPoint(dataPointId: number, updateMessage: UpdateDataPoint): Observable<DataPointResponse> {
    return this.putGeneric<UpdateDataPoint, DataPointResponse>(this.baseUrl + '/' + dataPointId, updateMessage);
  }

  setDataPointVisibility(dataPointId: number, dpIsVisible): Observable<DataPointResponse> {
    return this.putGeneric<void, DataPointResponse>(this.baseUrl + '/' + dataPointId + '/visibility/' + dpIsVisible, null);
  }

  getAllDataPointsFiltered(filter: Filter<DataPointFilter>): Observable<DataPointResponse[]> {
    filter.setPage(1);
    return this.getFilteredDataPoints(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredDataPoints(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }
}
