import {RelationalOperator} from './relational-operator';
import {AbstractFilter} from './abstract-filter';
import {Pagination} from './pagination';
import {SortDirection} from './sort-direction';

const DEFAULT_START_PAGE = 1;
const DEFAULT_PAGE_SIZE = 20;

export class Filter<T extends AbstractFilter> {

  private filter: T;
  private readonly pagination: Pagination;
  private readonly sort: Map<keyof T, SortDirection> = new Map<keyof T, SortDirection>();
  private readonly filterClass;

  constructor(filterClass: new() => T, page = DEFAULT_START_PAGE, pageSize = DEFAULT_PAGE_SIZE) {
    this.filterClass = filterClass;
    this.filter = new this.filterClass();
    this.pagination = new Pagination(page, pageSize);
  }

  public importFromQueryString(queryString: string): void {
    let queryStringSanitized = '';

    if (queryString.startsWith('?')) {
      queryStringSanitized = queryString.substring(1);
    } else {
      queryStringSanitized = queryString;
    }

    const splitted = queryStringSanitized.split('&'); // [sorts=...,filters=...,page=...,pageSize=...]
    splitted.forEach((queryElement) => {
      if (queryElement.startsWith('sorts=')) {
        const sortsString = queryElement.substring(6);
        const sortsSplitted = sortsString.split(',').map((s) => s.trim());
        sortsSplitted.forEach((sort) => {
          if (sort.startsWith('-')) {
            // @ts-ignore
            this.setSort(sort.substring(1), SortDirection.DESC);
          } else {
            // @ts-ignore
            this.setSort(sort, SortDirection.ASC);
          }
        });
      }
      if (queryElement.startsWith('filters=')) {
        const filtersString = queryElement.substring(8);
        const filtersSplitted = filtersString.split(',').map((f) => f.trim());
        filtersSplitted.forEach((filter) => {
          for (const operator of Object.values(RelationalOperator)) {
            const fSplitted = filter.split(operator);
            if (fSplitted.length === 2) {
              const key = fSplitted[0];
              let value: string | string[] = fSplitted[1];
              if (value.includes('|')) {
                value = value.split('|');
              }
              // @ts-ignore
              this.setFilter(key, value, operator as RelationalOperator);
              break;
            }
          }
        });

      }
      if (queryElement.startsWith('page=')) {
        this.setPage(parseInt(queryElement.substring(5), 10));
      }
      if (queryElement.startsWith('pageSize=')) {
        this.setPageSize(parseInt(queryElement.substring(9), 10));
      }
    });
  }

  public importFromFilter(filter: T): void {
    // TODO implement import
    throw new Error('Not implemented');
  }

  public importFromJsObject(): void {
    // TODO implement import
    throw new Error('Not implemented');
  }

  public getFilter<K extends keyof T>(key: K): T[K] {
    return this.filter[key];
  }

  public setPage(page: number): Filter<T> {
    this.pagination.page = page;
    return this;
  }

  public setPageSize(pageSize: number): Filter<T> {
    this.pagination.pageSize = pageSize;
    return this;
  }

  public setFilter<K extends keyof T>(
    key: K,
    operator: RelationalOperator,
    value: T[K] extends Map<RelationalOperator, infer V> ? V | V[] : never,
  ): Filter<T> {
    const x = this.filter[key];
    if (x instanceof Map) {
      x.set(operator, value);
    }
    return this;
  }

  public setSort(key: keyof T, value: SortDirection): Filter<T> {
    if (value === SortDirection.NONE) {
      this.sort.delete(key);
      return this;
    }
    this.sort.set(key, value);
    return this;
  }

  public getSort(key: keyof T): SortDirection {
    let v = this.sort.get(key);
    if (v === undefined) {
      v = SortDirection.NONE;
    }
    return v;
  }

  public removeSort(key: keyof T): boolean {
    return this.sort.delete(key);
  }

  public clearSort(): void {
    this.sort.clear();
  }

  public removeFilter(key: keyof T, operator: RelationalOperator): Filter<T> {
    const x = this.filter[key];
    if (x instanceof Map) {
      x.delete(operator);
    }
    return this;
  }

  public clearFilter(key: keyof T): Filter<T> {
    const x = this.filter[key];
    if (x instanceof Map) {
      x.clear();
    }
    return this;
  }

  public clearFilterCompletely(): Filter<T> {
    this.filter = new this.filterClass();
    return this;
  }

  public getQueryString(limiting: boolean): string {
    const queryParams: string[] = [];
    const queryParamsFilter: string[] = [];
    const queryParamsPagination: string[] = [];
    const queryParamsSort: string[] = [];
    const whereTupel = this.getFilledFilters();
    // create from pagination

    for (const [key, operator, value] of whereTupel) {
      let val = value;

      if (Array.isArray(value)) {
        val = value.join('|');
      }

      if (value instanceof Date) {
        val = value.toISOString();
      }

      queryParamsFilter.push(`${key}${operator}${val}`);
    }
    if (queryParamsFilter.length > 0) {
      queryParams.push('filters=' + queryParamsFilter.join(','));
    }

    for (const i of this.sort.keys()) {
      switch (this.sort.get(i)) {
        case SortDirection.ASC:
          queryParamsSort.push(i.toString());
          break;
        case SortDirection.DESC:
          queryParamsSort.push('-' + i);
          break;
      }
    }
    if (queryParamsSort.length > 0) {
      queryParams.push('sorts=' + queryParamsSort.join(','));
    }

    if (limiting) {
      queryParamsPagination.push(`page=${this.pagination.page}`);
      queryParamsPagination.push(`pageSize=${this.pagination.pageSize}`);
      queryParams.push(queryParamsPagination.join('&'));
    }

    return queryParams.length > 0 ? '?' + queryParams.join('&') : '';
  }

  public getPage(): number {
    return this.pagination.page;
  }

  private getFilledFilters(): Array<[string, RelationalOperator, any]> {
    const whereTupel: Array<[string, RelationalOperator, any]> = [];
    for (const i in this.filter) {
      if (this.filter.hasOwnProperty(i)) {
        const j = this.filter[i];
        if (j instanceof Map) {
          for (const [operator, value] of j.entries()) {
            whereTupel.push([i, operator, value]);
          }
        }
      }
    }
    return whereTupel;
  }
}
