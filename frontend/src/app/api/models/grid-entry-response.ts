import {DataPointResponse} from './data-point-response';
import {PictureResponse} from './picture-response';
import {SeriesResponse} from './series-response';

export interface GridEntryResponse {
  dataPoint?: DataPointResponse;
  id?: number;
  pictures?: PictureResponse[];
  series?: SeriesResponse;
}
