import {GridResponse} from './grid-response';

export interface SeriesResponse {
  grid?: GridResponse;
  id?: number;
  name?: null | string;
  order?: number;
  microIsVisible: boolean;
  macroIsVisible: boolean;
}
