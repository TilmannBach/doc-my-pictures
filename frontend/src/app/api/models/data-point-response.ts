import {GridResponse} from './grid-response';

export interface DataPointResponse {
  grid?: GridResponse;
  id?: number;
  name?: null | string;
  order?: number;
  isVisible: boolean;
}
