import {PictureResponse} from './picture-response';

export interface IndexedFileResponse {
  dateAdded?: string;
  directory?: null | string;
  fileName?: null | string;
  hidden?: boolean;
  id?: number;
  pictures?: null | Array<PictureResponse>;
}
