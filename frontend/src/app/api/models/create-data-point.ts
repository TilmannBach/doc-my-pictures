export interface CreateDataPoint {
  name?: null | string;
  order?: number;
  gridId?: number;
}
