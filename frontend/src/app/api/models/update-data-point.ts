export interface UpdateDataPoint {
  name?: null | string;
  order?: number;
  gridId?: number;
}
