export interface UpdateSeries {
  name?: null | string;
  order?: number;
  gridId?: number;
}
