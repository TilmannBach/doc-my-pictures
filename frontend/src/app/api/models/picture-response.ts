import {GridEntryResponse} from './grid-entry-response';
import {IndexedFileResponse} from './indexed-file-response';
import {PictureType} from './picture-type';

export interface PictureResponse {
  gridEntry?: GridEntryResponse;
  id?: number;
  indexedFile?: IndexedFileResponse;
  indexedFileId?: number;
  isFavorite?: boolean;
  order?: number;
  pictureType?: PictureType;
}
