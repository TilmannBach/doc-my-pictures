export interface CreateSeries {
  name?: null | string;
  order?: number;
  gridId?: number;
}
