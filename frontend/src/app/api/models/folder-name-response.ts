export interface FolderNameResponse {
  directory: string;
  countPictures: number;
}
