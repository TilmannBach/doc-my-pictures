import {PictureType} from './picture-type';

export interface AssignIndexedPicture {
  dataPointId: number;
  seriesId: number;
  indexedFileId: number;
  pictureType: PictureType;
  order?: number;
}
