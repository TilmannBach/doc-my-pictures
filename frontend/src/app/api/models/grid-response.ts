import {DataPointResponse} from './data-point-response';
import {GridEntryResponse} from './grid-entry-response';
import {SeriesResponse} from './series-response';

export interface GridResponse {
  dataPoints?: null | Array<DataPointResponse>;
  gridEntries?: null | Array<GridEntryResponse>;
  id?: number;
  name?: null | string;
  series?: null | Array<SeriesResponse>;
}
