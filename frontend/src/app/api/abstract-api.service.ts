import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {PagedResult} from './paged-result';
import {AbstractFilter} from './filter/abstract-filter';
import {Filter} from './filter/filter';

export abstract class AbstractApiService<T> {
  abstract baseUrl: string;
  abstract http: HttpClient;

  protected constructor() {
  }

  public getFiltered<X extends AbstractFilter>(url: string, filter: Filter<X>): Observable<PagedResult<T>> {
    const finalUrl = url + filter.getQueryString(true);
    const response = this.http.get<T[]>(finalUrl, {observe: 'response'});
    return response.pipe(
      map(r => {
        const pagedResult = new PagedResult<T>();
        pagedResult.currentPage = Number(r.headers.get('x-current-page'));
        pagedResult.pageCount = Number(r.headers.get('x-page-count'));
        pagedResult.pageSize = Number(r.headers.get('x-page-size'));
        pagedResult.rowCount = Number(r.headers.get('x-total-count'));
        pagedResult.results = r.body == null ? null : (r.body as T[]);
        return pagedResult;
      }),
    );
  }

  abstract fetch(filter: AbstractFilter): Observable<PagedResult<T>>;

  protected postGeneric<IN, OUT>(url: string, createMessage: IN): Observable<OUT> {
    return this.http.post<OUT>(url, createMessage);
  }

  protected putGeneric<IN, OUT>(url: string, updateMessage: IN): Observable<OUT> {
    return this.http.put<OUT>(url, updateMessage);
  }

  protected deleteGeneric<OUT>(url: string): Observable<OUT> {
    return this.http.delete<OUT>(url);
  }

  protected patchGeneric<IN, OUT>(url: string, patchMessage: IN | null = null): Observable<OUT> {
    return this.http.patch<OUT>(url, patchMessage);
  }
}
